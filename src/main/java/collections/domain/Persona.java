package collections.domain;

import java.util.Objects;

public class Persona implements Comparable<Persona>{

    private String dni;
    private String nom;
    private String cognoms;

    public Persona(String dni, String nom, String cognoms) {
        this.dni = dni;
        this.nom = nom;
        this.cognoms = cognoms;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getCognoms() {
        return cognoms;
    }

    public void setCognoms(String cognoms) {
        this.cognoms = cognoms;
    }


    public int compareToDavid(Persona o) {
        // TODO 3: Aqués métode e fa servir per comparar atributs d'objectes. Mira el compareTo de la classe String
        // JAUME: Està mal implementat: el métode compareTO compara dos objectes i ha de retornar el següent el que diu el métode de la interfície
        if (this.getDni() == o.getDni() && this.getClass() == o.getClass()){ // La comparació de getClass és innecessària
            return 0;
        }
        return 1;
    }

    @Override
    public int compareTo(Persona o) {
        return this.dni.compareTo(o.dni);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Persona persona = (Persona) o;
        return dni.equals(persona.dni);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dni);
    }

}
