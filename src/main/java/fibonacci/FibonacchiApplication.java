package fibonacci;

import java.util.logging.Level;
import java.util.logging.Logger;

public class FibonacchiApplication {

    public int factorial(int max){
        if(max == 0){
            Logger logger = Logger.getLogger("factorialLogger");
            logger.log(Level.INFO, "Soc al factorial");

            return 1;
        }else{
            return (max * factorial(max -1));
        }
    }

    public int initFibonacci(int init, int max){
        Logger logger = Logger.getLogger("initFibonacci Log");
        int result = 0;
        int n1 = 0;
        int n2 = 1;
        logger.log(Level.INFO, "n1: {0} ", String.valueOf(n1));
        logger.log(Level.INFO, "n2: {0} ", String.valueOf(n2));


        for (int i=init; i<max; i++){
            result = n1 + n2;
            n1 = n2;
            n2 = result;
            logger.log(Level.INFO, "result: {0} ", String.valueOf(result));
        }
        return result;
    }
}
