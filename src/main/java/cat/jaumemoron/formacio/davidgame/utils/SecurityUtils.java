package cat.jaumemoron.formacio.davidgame.utils;

import cat.jaumemoron.formacio.davidgame.domain.User;
import cat.jaumemoron.formacio.davidgame.security.UserPrincipal;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

public class SecurityUtils {

    public static String getUsername(){
        return getUser().getUsername();
    }

    public static String getId(){
        return getUser().getId();
    }

    public static String getRole(){
        return getUser().getRole();
    }

    public static boolean userHasRole(String role){
        return getRole().equals(role);
    }

    public static String getAlias(){
        return getUser().getAlias();
    }

    private static User getUser(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if ((authentication instanceof AnonymousAuthenticationToken) || authentication==null)
            throw  new AccessDeniedException("User is not logged");
        // Obtenim la principal de l'usuari
        UserPrincipal principal = (UserPrincipal) authentication.getPrincipal();
        return principal.getUser();
    }
}
