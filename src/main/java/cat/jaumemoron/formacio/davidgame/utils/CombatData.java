package cat.jaumemoron.formacio.davidgame.utils;

public class CombatData {
    Long dmgRecivedRol1;
    Long dmgRecivedRol2;
    Long healRecivedRol1;
    Long healRecivedRol2;

    public Long getDmgRecivedRol1() { return dmgRecivedRol1; }

    public void setDmgRecivedRol1(Long dmgRecivedRol1) { this.dmgRecivedRol1 = dmgRecivedRol1; }

    public Long getDmgRecivedRol2() { return dmgRecivedRol2; }

    public void setDmgRecivedRol2(Long dmgRecivedRol2) { this.dmgRecivedRol2 = dmgRecivedRol2; }

    public Long getHealRecivedRol1() { return healRecivedRol1; }

    public void setHealRecivedRol1(Long healRecivedRol1) { this.healRecivedRol1 = healRecivedRol1; }

    public Long getHealRecivedRol2() { return healRecivedRol2; }

    public void setHealRecivedRol2(Long healRecivedRol2) { this.healRecivedRol2 = healRecivedRol2; }
}
