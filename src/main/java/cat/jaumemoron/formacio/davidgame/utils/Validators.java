package cat.jaumemoron.formacio.davidgame.utils;

import cat.jaumemoron.formacio.davidgame.actions.ActionEngine;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

public class Validators {

    private static final Logger LOGGER = LoggerFactory.getLogger(ActionEngine.class);

    public static boolean isValidUsername(String username){
        boolean retval=true;
        if(StringUtils.isEmpty(username))
            retval =  false;
        if(username.length()>100)
            retval =  false;
        return retval;
    }

    public static boolean isValidPassword(String password){
        boolean retval=true;
        if(StringUtils.isEmpty(password))
            retval =  false;
        if(password.length()<2 || password.length()>60){
            retval = false;
        }
        return retval;
    }
    public static boolean isValidAlias(String alias){
        boolean retval=true;
        if(StringUtils.isEmpty(alias))
            retval =  false;
        if(alias.length()>20){
            retval = false;
        }
        return retval;
    }

    public static boolean roleTypeIsValid(String id, Long attackSucces, Long dodgeSucces, Long castHealSucces, Long interruptSucces, Long salut, Long armadura, Long mal, Long heal){
        boolean isValid=true;
        if (StringUtils.isEmpty(id) || attackSucces == null || attackSucces <= 0 ||
                dodgeSucces == null || dodgeSucces <= 0 ||
                castHealSucces == null || castHealSucces <= 0 ||
                interruptSucces == null || interruptSucces <= 0 ||
                salut == null || salut <= 0 ||
                armadura == null || armadura <= 0 ||
                mal == null || mal <= 0 ||
                heal == null || heal <= 0
        )
            isValid = false;
        return  isValid;
    }
}
