package cat.jaumemoron.formacio.davidgame.constants;

public enum UserStatus {
    PENDING_OF_ENROLLMENT,
    ACTIVE
}
