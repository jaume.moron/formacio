package cat.jaumemoron.formacio.davidgame.constants;

public enum ActionType {
    ATTACK,
    COUNTER,
    HEAL,
    INTERRUPT
}
