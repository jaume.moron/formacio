package cat.jaumemoron.formacio.davidgame.constants;

public enum Winner {
    PLAYER1,
    PLAYER2,
    DRAW
}
