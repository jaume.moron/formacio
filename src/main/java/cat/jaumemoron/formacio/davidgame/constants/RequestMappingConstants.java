package cat.jaumemoron.formacio.davidgame.constants;

public class RequestMappingConstants {

    public static final String ROOT_REQUEST_MAPPING = "/";
    public static final String INIT_ROOT_REQUEST_MAPPING = "index.html";

    public static final String REGISTER_REQUEST_MAPPING = "/register";
    public static final String INIT_REGISTER_REQUEST_MAPPING = "register/init.html";
    public static final String END_REGISTER_REQUEST_MAPPING = "register/end.html";

    public static final String TOKEN_REQUEST_MAPPING = "/{token}";

    public static final String LOGIN_REQUEST_MAPPING = "/login";
    public static final String INIT_LOGIN_REQUEST_MAPPING = "login.html";

    public static final String API_REQUEST_MAPPING = "/api";
    public static final String ROLE_API_REQUEST_MAPPING = "/role";
    public static final String CHARACTER_REQUEST_MAPPING = "/character";

    public static final String COMBAT_REQUEST_MAPPING = "/combats";
    public static final String START_HTML_MAPPING = "combats/start.html";
    public static final String INIT_COMBAT_REQUEST_MAPPING = "combats/init.html";


    public static final String CHARACTERS_REQUEST_MAPPING = "/characters";
    public static final String INIT_CHARACTERS_REQUEST_MAPPING = "characters/init.html";
    public static final String CREATE_CHARACTERS_REQUEST_MAPPING = "characters/crud/create.html";

    public static final String ROLES_REQUEST_MAPPING = "/roles";
    public static final String INIT_ROLES_REQUEST_MAPPING = "roles/init.html";
    public static final String CREATE_ROLES_REQUEST_MAPPING = "roles/crud/create.html";

    public static final String CRUD_MAPPING = "/crud";
    public static final String ID_REQUEST_MAPPING = "/{id}";

    public static final String USER_REQUEST_MAPPING = "/user";

    public static final String CONTENT_REQUEST_MAPPING = "content";

    public static final String USERS_REQUEST_MAPPING = "/users";
    public static final String INIT_USER_REQUEST_MAPPING = "users/init.html";

    public static final String COMBATENGINE_REQUEST_MAPPING = "/combatEngine";

}
