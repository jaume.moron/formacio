package cat.jaumemoron.formacio.davidgame.constants;

public enum CombatStatus {
    PENDING,
    IN_PROCESS,
    FINISHED
}
