package cat.jaumemoron.formacio.davidgame.constants;

public class SecurityConstants {

    public static final String ROLE_PREFIX = "ROLE_";
    public static final String ADMIN = "ADMIN";
    public static final String USER = "USER";
    public static final String ADMIN_ROLE = ROLE_PREFIX + ADMIN;
    public static final String USER_ROLE = ROLE_PREFIX + USER;

}