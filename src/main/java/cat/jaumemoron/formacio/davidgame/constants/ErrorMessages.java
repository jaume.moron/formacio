package cat.jaumemoron.formacio.davidgame.constants;

public class ErrorMessages {
    public static final String ARGUMENT_EXCEPTION_ID_NULL = "Id cannot be null";

    public static final String ARGUMENT_EXCEPTION_ROLE_NULL = "Rol cannot be null";
    public static final String ARGUMENT_EXCEPTION_ROL1_NULL = "Rol1 cannot be null";
    public static final String ARGUMENT_EXCEPTION_CHAR2_NULL = "Rol2 cannot be null";

    public static final String ARGUMENT_EXCEPTION_TORN_NULL = "Torn cannot be null";
    public static final String ARGUMENT_EXCEPTION_COMBAT_NULL = "Combat cannot be null";
    public static final String ARGUMENT_EXCEPTION_ACCIO_NULL = "Accio cannot be null";

    public static final String ARGUMENT_EXCEPTION_USERNAME_EMPTY = "Username cannot be null";
    public static final String ARGUMENT_EXCEPTION_CONTRA_EMPTY = "UserPassword cannot be empty";
    public static final String ARGUMENT_EXCEPTION_ALIAS_EMPTY = "Alias cannot be null";

    public static final String ARGUMENT_EXCEPTION_ACTIONTYPE_NULL = "actionType cannot be empty";

    public static final String USER_DOES_NOT_EXISTS = "User does not exists";
    public static final String USER_STATUS_IS_ACTIVE = "User status is already active";

    public static final String ENROLLMENT_TOKEN_IS_NULLL = "Enrollment code cannot be null";
    public static final String ENROLLMENT_DATE_IS_NULLL = "Enrollment date cannot be null";
    public static final String ENROLLMENT_IS_NULLL = "Enrollment object cannot be null";
    public static final String ENROLLMENT_DOES_NOT_EXISTS = "Enrollment token does not exists";

    public static final String USER_ALREADY_EXISTS_PARAM_USERNAME = "User [%s] already exists";
    public static final String USERNAME_LENGTH_PARAM_USERNAME = "Username [%s] contains an invalid format";
    public static final String CONTRA_LENGTH = "Password contains an invalid format";
    public static final String ALIAS_LENGTH = "Alias contains an invalid format, max 20 characters";

    public static final String ARGUMENT_EXCEPTION_INCORRECT_PARAM = "Incorrect param, null or < 0";


}
