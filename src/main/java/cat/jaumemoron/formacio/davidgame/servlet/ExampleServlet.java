package cat.jaumemoron.formacio.davidgame.servlet;

import cat.jaumemoron.formacio.davidgame.constants.CombatStatus;
import cat.jaumemoron.formacio.davidgame.domain.Combat;
import cat.jaumemoron.formacio.davidgame.service.CombatsService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet("/listCombatsServlet")
public class ExampleServlet extends HttpServlet {

    @Autowired
    private static CombatsService combatsService;

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        try {
            doPost(req, resp);
        } catch (Exception e) {
            resp.setStatus(500);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        try {
            PrintWriter writer = resp.getWriter();
            resp.setCharacterEncoding("UTF-8");
            resp.setStatus(200);
            writer.println("Això és un exemple de servlet que ens xiva els combats en process del joc");

            List<Combat> allCombats = combatsService.findCombatByStatus(CombatStatus.IN_PROCESS);
            for (Combat allCombat : allCombats) {
                writer.println("combat en proces id:" + allCombat.getId());
            }
        } catch (IOException e) {
            resp.setStatus(500);
        }

    }

}
