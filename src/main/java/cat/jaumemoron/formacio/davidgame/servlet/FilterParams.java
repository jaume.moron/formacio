package cat.jaumemoron.formacio.davidgame.servlet;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

@WebFilter(filterName="/FilterParams", urlPatterns = "/*") //  "/exemple" per que el filtre actui només sobre el servlet, ja que es la ruta del servlet
public class FilterParams implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        HttpServletRequest req = (HttpServletRequest) servletRequest;

        String sessionId = req.getSession().getId();
        Map parameters = req.getParameterMap();

        if (!parameters.isEmpty()){
            Set entrySet = parameters.entrySet();
            Iterator iterator = entrySet.iterator();

            while(iterator.hasNext()) {

                Map.Entry<String, String[]> entry = (Map.Entry<String, String[]>) iterator.next();
                String key = entry.getKey();
                String[] value = entry.getValue();
                StringBuilder sb = new StringBuilder();

                if(value.length>1){
                    for (int i = 0; i < value.length; i++) {
                        sb.append(value[i]);
                    }
                }else{
                    sb.append(value[0]);
                }
                System.out.println("Sessió ->" + sessionId + " parametre ->" + key + " te el valor -> " + sb );

            }

        }
        else {
            System.out.println("no te paramateres");
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }
}
