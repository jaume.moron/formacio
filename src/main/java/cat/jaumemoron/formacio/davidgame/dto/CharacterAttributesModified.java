package cat.jaumemoron.formacio.davidgame.dto;

import cat.jaumemoron.formacio.davidgame.domain.CharacterAttributes;

public class CharacterAttributesModified extends CharacterAttributes {


    public static final class Builder {
        private Long salut;
        private Long armadura;
        private Long mal;
        private Long heal;

        private Builder() {
        }

        public static Builder aCharacterAttributesModified() {
            return new Builder();
        }

        public Builder withSalut(Long salut) {
            this.salut = salut;
            return this;
        }

        public Builder withArmadura(Long armadura) {
            this.armadura = armadura;
            return this;
        }

        public Builder withMal(Long mal) {
            this.mal = mal;
            return this;
        }

        public Builder withHeal(Long heal) {
            this.heal = heal;
            return this;
        }

        public CharacterAttributesModified build() {
            CharacterAttributesModified characterAttributesModified = new CharacterAttributesModified();
            characterAttributesModified.setSalut(salut);
            characterAttributesModified.setArmadura(armadura);
            characterAttributesModified.setMal(mal);
            characterAttributesModified.setHeal(heal);
            return characterAttributesModified;
        }
    }
}
