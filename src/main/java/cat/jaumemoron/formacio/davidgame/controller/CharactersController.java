package cat.jaumemoron.formacio.davidgame.controller;

import cat.jaumemoron.formacio.davidgame.constants.RequestMappingConstants;
import cat.jaumemoron.formacio.davidgame.domain.RoleType;
import cat.jaumemoron.formacio.davidgame.domain.User;
import cat.jaumemoron.formacio.davidgame.dto.CharacterAttributesModified;
import cat.jaumemoron.formacio.davidgame.service.CharactersService;
import cat.jaumemoron.formacio.davidgame.service.RoleTypesService;
import cat.jaumemoron.formacio.davidgame.service.UsersService;
import cat.jaumemoron.formacio.davidgame.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import static cat.jaumemoron.formacio.davidgame.constants.ErrorMessages.USER_DOES_NOT_EXISTS;
import static cat.jaumemoron.formacio.davidgame.constants.RequestMappingConstants.*;

@Controller
@RequestMapping(CHARACTERS_REQUEST_MAPPING)
public class CharactersController {

    @Autowired
    CharactersService charactersService;

    @Autowired
    UsersService usersService;

    @Autowired
    RoleTypesService roleTypesService;

    @GetMapping
    public String init(Model model) {

        User userLogged = usersService.findByUsername(SecurityUtils.getUsername());
        if (userLogged == null) {
            throw new IllegalArgumentException(USER_DOES_NOT_EXISTS);
        }
        if (userLogged.getRole().equals("ROLE_ADMIN")) {
            model.addAttribute("isAdmin", "is Admin");
            model.addAttribute("characters", charactersService.findAll());
        }

        return RequestMappingConstants.INIT_CHARACTERS_REQUEST_MAPPING;
    }

    @GetMapping(value = CRUD_MAPPING)
    public String create() {
        return CREATE_CHARACTERS_REQUEST_MAPPING;
    }
    @PostMapping(value = CRUD_MAPPING)
    public String create(@RequestParam("characterName") String characterName,
                         @RequestParam("comboRoleTypes") String id,
                            Model model) {

        RoleType roletype = roleTypesService.findById(id);
        //TODO fer la part dels atribut escollit per l'usuari
        CharacterAttributesModified attributes = new CharacterAttributesModified();

        try{
            charactersService.create(SecurityUtils.getId(), roletype, characterName, attributes );
            //Character create(Long idUser, RoleType roleType, String name, CharacterAttributesModified attributes);
            return INIT_CHARACTERS_REQUEST_MAPPING;
        }catch (IllegalArgumentException e){
            model.addAttribute("errorMessage", e);
            return INIT_CHARACTERS_REQUEST_MAPPING;
        }
    }
}

