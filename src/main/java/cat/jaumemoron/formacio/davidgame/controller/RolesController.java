package cat.jaumemoron.formacio.davidgame.controller;

import cat.jaumemoron.formacio.davidgame.constants.RequestMappingConstants;
import cat.jaumemoron.formacio.davidgame.service.RoleTypesService;
import cat.jaumemoron.formacio.davidgame.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import static cat.jaumemoron.formacio.davidgame.constants.RequestMappingConstants.*;

@Controller
@RequestMapping(ROLES_REQUEST_MAPPING)
public class RolesController {

    @Autowired
    RoleTypesService roleTypesService;

    @GetMapping
    public String init() {
        return RequestMappingConstants.INIT_ROLES_REQUEST_MAPPING;
    }

    @GetMapping(value = CRUD_MAPPING)
    public String create() {
        return CREATE_ROLES_REQUEST_MAPPING;
    }

    @PostMapping(value = CRUD_MAPPING)
    public String create(@RequestParam("name") String name,
                         @RequestParam("attackSucces") Long attackSucces,
                         @RequestParam("dodgeSucces") Long dodgeSucces,
                         @RequestParam("castHealSucces") Long castHealSucces,
                         @RequestParam("interruptSucces") Long interruptSucces,
                         @RequestParam("salut") Long salut,
                         @RequestParam("armadura") Long armadura,
                         @RequestParam("mal") Long mal,
                         @RequestParam("heal") Long heal,
                         Model model) {
        try{
            roleTypesService.create(name, attackSucces,dodgeSucces,castHealSucces,interruptSucces,salut,armadura,mal,heal);
            return INIT_ROLES_REQUEST_MAPPING;
        }catch (IllegalArgumentException e){
            model.addAttribute("errorMessage", e);
            return INIT_ROLES_REQUEST_MAPPING;
        }

    }
    @PutMapping(value = ID_REQUEST_MAPPING)
    public String update (@RequestParam("id") String id,
                          @RequestParam("attackSucces") Long attackSucces,
                          @RequestParam("dodgeSucces") Long dodgeSucces,
                          @RequestParam("castHealSucces") Long castHealSucces,
                          @RequestParam("interruptSucces") Long interruptSucces,
                          @RequestParam("salut") Long salut,
                          @RequestParam("armadura") Long armadura,
                          @RequestParam("mal") Long mal,
                          @RequestParam("heal") Long heal,
                          Model model) {
        try{
            roleTypesService.update(id, attackSucces,dodgeSucces,castHealSucces,interruptSucces,salut,armadura,mal,heal);
            return INIT_ROLES_REQUEST_MAPPING;
        }catch (IllegalArgumentException e){
            model.addAttribute("errorMessage", e);
            return INIT_ROLES_REQUEST_MAPPING;
        }
    }

}
