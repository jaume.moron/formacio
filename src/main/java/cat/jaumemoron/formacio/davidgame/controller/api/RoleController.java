package cat.jaumemoron.formacio.davidgame.controller.api;

import cat.jaumemoron.formacio.davidgame.constants.RequestMappingConstants;
import cat.jaumemoron.formacio.davidgame.domain.RoleType;
import cat.jaumemoron.formacio.davidgame.dto.RestApiError;
import cat.jaumemoron.formacio.davidgame.service.RoleTypesService;
import cat.jaumemoron.formacio.davidgame.utils.Validators;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static cat.jaumemoron.formacio.davidgame.constants.ErrorMessages.ARGUMENT_EXCEPTION_INCORRECT_PARAM;

@RestController
@RequestMapping(RequestMappingConstants.API_REQUEST_MAPPING + RequestMappingConstants.ROLE_API_REQUEST_MAPPING)
@Api(description = "Services to manage roles", tags = "Role services")
public class RoleController {

    @Autowired
    private RoleTypesService service;

    @GetMapping
    @ApiOperation(value = "Get all roles", response = RoleType.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The role list", response = RoleType.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Invalid parameter", response = RestApiError.class),
            @ApiResponse(code = 401, message = "User is not logged", response = RestApiError.class),
            @ApiResponse(code = 403, message = "User not authorized", response = RestApiError.class)}
    )
    @ResponseStatus(value = HttpStatus.OK)
    public List<RoleType> findAll() {
        return service.findAll();
    }

    @PostMapping
    @ApiOperation(value = "Create a new role", response = RoleType.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The new role created", response = RoleType.class),
            @ApiResponse(code = 400, message = "Invalid parameter", response = RestApiError.class),
            @ApiResponse(code = 401, message = "User is not logged", response = RestApiError.class),
            @ApiResponse(code = 403, message = "User not authorized", response = RestApiError.class)}
    )
    @ResponseStatus(value = HttpStatus.CREATED)
    public RoleType create(@ApiParam(value = "The role name", required = true, example = "1")
                           @RequestParam("name") String name,
                           @ApiParam(value = "The percent of probability that an attack action will be successfully", required = true, example = "1")
                           @RequestParam("attackSucces") Long attackSucces,
                           @ApiParam(value = "The percent of probability that a dodge action will be successfully", required = true, example = "1")
                           @RequestParam("dodgeSucces") Long dodgeSucces,
                           @ApiParam(value = "The percent of probability that a heal action will be successfully", required = true, example = "1")
                           @RequestParam("castHealSucces") Long castHealSucces,
                           @ApiParam(value = "The percent of probability that an interrupt action will be successfully", required = true, example = "1")
                           @RequestParam("interruptSucces") Long interruptSucces,
                           @ApiParam(value = "The amount of heal", required = true, example = "1")
                           @RequestParam("salut") Long salut,
                           @ApiParam(value = "The amount of armor", required = true, example = "1")
                           @RequestParam("armadura") Long armadura,
                           @ApiParam(value = "The amount of damage", required = true, example = "1")
                           @RequestParam("mal") Long mal,
                           @ApiParam(value = "The amount of heal that will be restored if a heal action sucess", required = true, example = "1")
                           @RequestParam("heal") Long heal) {
        //TODO validar que sigui mes gran de 5 caracter, si es mes petit enviar un argument illegan exception

        return service.create(name, attackSucces, dodgeSucces, castHealSucces, interruptSucces, salut, armadura, mal, heal);
    }

    @PutMapping(value = "/{id}")
    @ApiOperation(value = "Update a role attributes", response = RoleType.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The role instance updated", response = RoleType.class),
            @ApiResponse(code = 400, message = "Invalid parameter", response = RestApiError.class),
            @ApiResponse(code = 401, message = "User is not logged", response = RestApiError.class),
            @ApiResponse(code = 403, message = "User not authorized", response = RestApiError.class)}
    )
    @ResponseStatus(value = HttpStatus.OK)
    public RoleType update(@PathVariable("id") String id,
                           @ApiParam(value = "The percent of probability that an attack action will be successfully", required = true, example = "1")
                           @RequestParam("attackSucces") Long attackSucces,
                           @ApiParam(value = "The percent of probability that a dodge action will be successfully", required = true, example = "1")
                           @RequestParam("dodgeSucces") Long dodgeSucces,
                           @ApiParam(value = "The percent of probability that a heal action will be successfully", required = true, example = "1")
                           @RequestParam("castHealSucces") Long castHealSucces,
                           @ApiParam(value = "The percent of probability that an interrupt action will be successfully", required = true, example = "1")
                           @RequestParam("interruptSucces") Long interruptSucces,
                           @ApiParam(value = "The amount of heal", required = true, example = "1")
                           @RequestParam("salut") Long salut,
                           @ApiParam(value = "The amount of armor", required = true, example = "1")
                           @RequestParam("armadura") Long armadura,
                           @ApiParam(value = "The amount of damate", required = true, example = "1")
                           @RequestParam("mal") Long mal,
                           @ApiParam(value = "The amount of heal that will be restored if a heal action sucess", required = true, example = "1")
                           @RequestParam("heal") Long heal) {
        // TODO: Implementar métode per a modificar un tipus de rol
        if(!Validators.roleTypeIsValid(id, attackSucces, dodgeSucces, castHealSucces, interruptSucces, salut, armadura, mal, heal)){
            throw new IllegalArgumentException(ARGUMENT_EXCEPTION_INCORRECT_PARAM);
        }
        RoleType updatedRoleType = service.update(id, attackSucces, dodgeSucces, castHealSucces, interruptSucces, salut, armadura, mal, heal);
        return updatedRoleType;
    }

    @DeleteMapping(value = "/{id}")
    @ApiOperation(value = "Delete a role", response = RoleType.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The role has been deleted"),
            @ApiResponse(code = 400, message = "Invalid parameter", response = RestApiError.class),
            @ApiResponse(code = 401, message = "User is not logged", response = RestApiError.class),
            @ApiResponse(code = 403, message = "User not authorized", response = RestApiError.class)}
    )
    @ResponseStatus(value = HttpStatus.OK)
    public void delete(@PathVariable("id") String id) throws IllegalArgumentException  {
        service.deleteById(id);
    }

}
