package cat.jaumemoron.formacio.davidgame.controller;

import cat.jaumemoron.formacio.davidgame.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import static cat.jaumemoron.formacio.davidgame.constants.RequestMappingConstants.INIT_USER_REQUEST_MAPPING;
import static cat.jaumemoron.formacio.davidgame.constants.RequestMappingConstants.USERS_REQUEST_MAPPING;

@Controller
@RequestMapping(USERS_REQUEST_MAPPING)
public class UsersController {

    @Autowired
    UsersService service;

    @GetMapping
    public String init() {
        return INIT_USER_REQUEST_MAPPING;
    }
}


