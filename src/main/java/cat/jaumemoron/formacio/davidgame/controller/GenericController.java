package cat.jaumemoron.formacio.davidgame.controller;

import cat.jaumemoron.formacio.davidgame.constants.RequestMappingConstants;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class GenericController {

    @GetMapping(value = RequestMappingConstants.ROOT_REQUEST_MAPPING)
    public String root() {
        return RequestMappingConstants.INIT_ROOT_REQUEST_MAPPING;
    }

    @GetMapping(value = RequestMappingConstants.LOGIN_REQUEST_MAPPING)
    public String login() {
        return RequestMappingConstants.INIT_LOGIN_REQUEST_MAPPING;
    }

}
