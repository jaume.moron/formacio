package cat.jaumemoron.formacio.davidgame.controller.api;

import cat.jaumemoron.formacio.davidgame.dto.RestApiError;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class ApiErrorHandler {

    @ExceptionHandler(AccessDeniedException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ResponseBody
    public RestApiError handle(AccessDeniedException exception) {
        return new RestApiError(HttpStatus.UNAUTHORIZED, "Access denied", exception.getLocalizedMessage());
    }

    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public RestApiError handle(IllegalArgumentException exception) {
        return new RestApiError(HttpStatus.BAD_REQUEST, "Bad request", exception.getLocalizedMessage());
    }
}
