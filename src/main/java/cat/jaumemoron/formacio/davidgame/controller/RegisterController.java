package cat.jaumemoron.formacio.davidgame.controller;

import cat.jaumemoron.formacio.davidgame.adapter.UsersAdapter;
import cat.jaumemoron.formacio.davidgame.constants.RequestMappingConstants;
import cat.jaumemoron.formacio.davidgame.exceptions.UserExistsException;
import cat.jaumemoron.formacio.davidgame.utils.Validators;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import static cat.jaumemoron.formacio.davidgame.constants.ErrorMessages.*;

@Controller
@RequestMapping(RequestMappingConstants.REGISTER_REQUEST_MAPPING)
public class RegisterController {

    @Autowired
    private UsersAdapter adapter;

    @GetMapping
    public String init() {
        return RequestMappingConstants.INIT_REGISTER_REQUEST_MAPPING;
    }

    @PostMapping
    public String register(@RequestParam("username") String username,
                           @RequestParam("password") String password,
                           @RequestParam("alias") String alias,
                           Model model) {

        // TODO: Validar tamany username, password i alias
        // TODO: Si el password ha de tenir un format en concret, validar-ho aquí també
        // Cridem a l'adapter per a registrar l'usuari

        if(!Validators.isValidUsername(username)){
            model.addAttribute("errorMessage", String.format(USERNAME_LENGTH_PARAM_USERNAME, username));
        }
        if(!Validators.isValidPassword(password)){
            model.addAttribute("errorMessage", CONTRA_LENGTH);
        }
        if(!Validators.isValidAlias(alias)){
            model.addAttribute("errorMessage", ALIAS_LENGTH);
        }
        if(model.containsAttribute("errorMessage")){
            return RequestMappingConstants.INIT_REGISTER_REQUEST_MAPPING;
        }else {
            try{
                adapter.userRegistration(username, password, alias, model);
                return RequestMappingConstants.END_REGISTER_REQUEST_MAPPING;
            }catch (UserExistsException e){
                model.addAttribute("errorMessage", e);
                return RequestMappingConstants.INIT_REGISTER_REQUEST_MAPPING;
            }
        }
    }

    @GetMapping(value = RequestMappingConstants.TOKEN_REQUEST_MAPPING)
    public String endUserRegistration(@PathVariable("token") String token, Model model) {
        // Cridem a l'adapter per a registrar l'usuari
        adapter.endUserRegistration(token);
        model.addAttribute("message", "Registre completat");
        return RequestMappingConstants.INIT_LOGIN_REQUEST_MAPPING;
    }
}
