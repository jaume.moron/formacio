package cat.jaumemoron.formacio.davidgame.controller.api;

import cat.jaumemoron.formacio.davidgame.constants.RequestMappingConstants;
import cat.jaumemoron.formacio.davidgame.domain.User;
import cat.jaumemoron.formacio.davidgame.dto.RestApiError;
import cat.jaumemoron.formacio.davidgame.service.UsersService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(RequestMappingConstants.API_REQUEST_MAPPING + RequestMappingConstants.USER_REQUEST_MAPPING)
@Api(description = "Services to manage users", tags = "User services")
public class UserController {

    @Autowired
    UsersService service;

    @GetMapping
    @ApiOperation(value = "Get all users", response = User.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The user list", response = User.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Invalid parameter", response = RestApiError.class),
            @ApiResponse(code = 401, message = "User is not logged", response = RestApiError.class),
            @ApiResponse(code = 403, message = "User not authorized", response = RestApiError.class)}
    )
    @ResponseStatus(value = HttpStatus.OK)
    public List<User> findAll() {
        return service.findAll();
    }

    @DeleteMapping(value = "/{id}")
    @ApiOperation(value = "Delete a user", response = User.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "User has been deleted"),
            @ApiResponse(code = 400, message = "Invalid parameter", response = RestApiError.class),
            @ApiResponse(code = 401, message = "User is not logged", response = RestApiError.class),
            @ApiResponse(code = 403, message = "User not authorized", response = RestApiError.class)}
    )
    @ResponseStatus(value = HttpStatus.OK)
    public void delete(@PathVariable("id") String id) throws IllegalArgumentException  {
        service.deleteById(id);
    }

}
