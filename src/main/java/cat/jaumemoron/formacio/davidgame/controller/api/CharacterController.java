package cat.jaumemoron.formacio.davidgame.controller.api;

import cat.jaumemoron.formacio.davidgame.constants.RequestMappingConstants;
import cat.jaumemoron.formacio.davidgame.domain.Character;
import cat.jaumemoron.formacio.davidgame.dto.RestApiError;
import cat.jaumemoron.formacio.davidgame.service.CharactersService;
import cat.jaumemoron.formacio.davidgame.utils.SecurityUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(RequestMappingConstants.API_REQUEST_MAPPING + RequestMappingConstants.CHARACTER_REQUEST_MAPPING)
@Api(description = "Services to manage characters", tags = "Character services")
public class CharacterController {

    @Autowired
    private CharactersService charactersService;

    @GetMapping
    @ApiOperation(value = "Get all characters", response = Character.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The character list", response = Character.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Invalid parameter", response = RestApiError.class),
            @ApiResponse(code = 401, message = "User is not logged", response = RestApiError.class),
            @ApiResponse(code = 403, message = "User not authorized", response = RestApiError.class)}
    )
    @ResponseStatus(value = HttpStatus.OK)
    public List<Character> findAll() { return charactersService.findAll(); }

    @GetMapping(value = RequestMappingConstants.USER_REQUEST_MAPPING)
    @ApiOperation(value = "Get all logged characters", response = Character.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The logged character list", response = Character.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Invalid parameter", response = RestApiError.class),
            @ApiResponse(code = 401, message = "User is not logged", response = RestApiError.class),
            @ApiResponse(code = 403, message = "User not authorized", response = RestApiError.class)}
    )
    @ResponseStatus(value = HttpStatus.OK)
    public List<Character> findLoggedChars() {
        String id = SecurityUtils.getId();

        if(SecurityUtils.userHasRole("ROLE_ADMIN")){
            return findAll();
        }
        return charactersService.findByIdUser(id);
    }

}
