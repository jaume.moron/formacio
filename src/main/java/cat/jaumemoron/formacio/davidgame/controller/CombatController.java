package cat.jaumemoron.formacio.davidgame.controller;

import cat.jaumemoron.formacio.davidgame.domain.Character;
import cat.jaumemoron.formacio.davidgame.domain.Combat;
import cat.jaumemoron.formacio.davidgame.service.CharactersService;
import cat.jaumemoron.formacio.davidgame.service.CombatsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import static cat.jaumemoron.formacio.davidgame.constants.ErrorMessages.USERNAME_LENGTH_PARAM_USERNAME;
import static cat.jaumemoron.formacio.davidgame.constants.RequestMappingConstants.*;

@Controller
@RequestMapping(COMBAT_REQUEST_MAPPING)
public class CombatController {

    @Autowired
    private CombatsService combatsService;
    @Autowired
    private CharactersService charactersService;

    @GetMapping
    public String init() {
        return INIT_COMBAT_REQUEST_MAPPING;
    }

    @PostMapping
    public String start(@RequestParam("charsDropDown") String idChar,
                        @RequestParam("idEnemy") String idEnemy,
                        Model model) {
        Character fighter = charactersService.findById(idChar);
        Character enemy = charactersService.findById(idEnemy);

        Combat combat = combatsService.create(fighter, enemy);

        model.addAttribute("fighter", fighter);
        model.addAttribute("enemy", enemy);
        model.addAttribute("combat_id", combat.getId());

        return START_HTML_MAPPING;
    }
}
