package cat.jaumemoron.formacio.davidgame.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import static cat.jaumemoron.formacio.davidgame.constants.RequestMappingConstants.*;

@Controller
@RequestMapping(CONTENT_REQUEST_MAPPING)
public class ContentController {

    @RequestMapping("")
    public String loadContent() {
        return "fragments/crud :: edit";
    }
}