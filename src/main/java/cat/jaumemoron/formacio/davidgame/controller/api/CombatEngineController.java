package cat.jaumemoron.formacio.davidgame.controller.api;

import cat.jaumemoron.formacio.davidgame.constants.ActionType;
import cat.jaumemoron.formacio.davidgame.constants.CombatStatus;
import cat.jaumemoron.formacio.davidgame.constants.RequestMappingConstants;
import cat.jaumemoron.formacio.davidgame.domain.Combat;
import cat.jaumemoron.formacio.davidgame.domain.Torn;
import cat.jaumemoron.formacio.davidgame.dto.RestApiError;
import cat.jaumemoron.formacio.davidgame.service.CombatEngineService;
import cat.jaumemoron.formacio.davidgame.service.CombatsService;
import cat.jaumemoron.formacio.davidgame.service.TornsService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.security.SecureRandom;

@RestController
@RequestMapping(RequestMappingConstants.API_REQUEST_MAPPING + RequestMappingConstants.COMBATENGINE_REQUEST_MAPPING)
@Api(description = "Services to manage combat", tags = "Combat services")
public class CombatEngineController {

    @Autowired
    CombatEngineService service;
    @Autowired
    CombatsService combatService;

    @GetMapping
    @ApiOperation(value = "return turn result", response = Torn.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "New turn data", response = Torn.class),
            @ApiResponse(code = 400, message = "Invalid parameter", response = RestApiError.class),
            @ApiResponse(code = 401, message = "User is not logged", response = RestApiError.class),
            @ApiResponse(code = 403, message = "User not authorized", response = RestApiError.class)}
    )
    @ResponseStatus(value = HttpStatus.OK)
    public Torn accioResult(@ApiParam(value = "Id combat", required = true, example = "123e4567-e89b-12d3-a456-556642440000")
                           @RequestParam("id_combat") String id_combat,
                           @ApiParam(value = "Action fighter", required = true, example = "1")
                           @RequestParam("actionFighter") Integer action1)
    {
        Combat combat = combatService.findById(id_combat);
        //mirem si esta en pending, si ho esta el posem com en proces
        if(combat.getStatus().equals(CombatStatus.PENDING)){
            combat = combatService.setCombatStatus(combat, CombatStatus.IN_PROCESS);
        }
        ActionType fighterAction = getActionType(action1);
        //TODO: la IA nomes va random de moment
        ActionType iaAction = getRandomAction();
        //END IA RANDOM


        Torn torn = null;
        torn = service.accioResult(combat, fighterAction, iaAction);

        if(service.isCombatFinished(torn)){
            combatService.setCombatStatus(combat, CombatStatus.FINISHED);
        };

        return torn;
    }

    private ActionType getRandomAction(){
        ActionType actionType = null;
        SecureRandom rand = new SecureRandom(); //instance of random class
        int upperbound = 100;
        int intRandom = rand.nextInt(upperbound) + 1;
        if(intRandom <= 25){
            actionType = ActionType.ATTACK;
        }
        if(intRandom > 25 && intRandom <= 50){
            actionType = ActionType.COUNTER;
        }
        if(intRandom > 50 && intRandom <= 75){
            actionType = ActionType.HEAL;
        }
        if(intRandom > 75 && intRandom <= 100){
            actionType = ActionType.INTERRUPT;
        }
        return actionType;
    }

    private ActionType getActionType(Integer action){
        switch (action){
            case 1:
                return ActionType.ATTACK;
            case 2:
                return ActionType.COUNTER;
            case 3:
                return ActionType.HEAL;
            case 4:
                return ActionType.INTERRUPT;
        }
        return null;
    }

}
