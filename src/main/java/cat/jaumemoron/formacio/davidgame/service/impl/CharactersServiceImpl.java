package cat.jaumemoron.formacio.davidgame.service.impl;

import cat.jaumemoron.formacio.davidgame.domain.Character;
import cat.jaumemoron.formacio.davidgame.domain.RoleType;
import cat.jaumemoron.formacio.davidgame.dto.CharacterAttributesModified;
import cat.jaumemoron.formacio.davidgame.repository.CharactersRepository;
import cat.jaumemoron.formacio.davidgame.service.CharactersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.HttpRequestHandler;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

import static cat.jaumemoron.formacio.davidgame.constants.ErrorMessages.ARGUMENT_EXCEPTION_ID_NULL;

@Service
public class CharactersServiceImpl implements CharactersService {

    @Autowired
    private CharactersRepository repository;

    @Override
    public Character findById(String id) {
        if(id == null){
            throw new IllegalArgumentException(ARGUMENT_EXCEPTION_ID_NULL);
        }
        Optional<Character> optional = repository.findById(id);
        return optional.orElse(null);
    }

    @Override
    public Character create(String idUser, RoleType roleType, String name, CharacterAttributesModified attributes) {
        if (StringUtils.isEmpty(idUser))
            throw new IllegalArgumentException("idUser cannot be empty");
        if (StringUtils.isEmpty(roleType))
            throw new IllegalArgumentException("rolType cannot be empty");
        if (StringUtils.isEmpty(name))
            throw new IllegalArgumentException("name cannot be empty");
        Character character = new Character();
        character.setName(name);
        character.setIdUser(idUser);
        character.setRoleType(roleType);
        // Actualitzem els atributs per defecte amb els escollits per l'usuari
        character.setArmadura(sum(roleType.getArmadura(), attributes.getArmadura()));
        character.setHeal(sum(roleType.getHeal(), attributes.getHeal()));
        character.setSalut(sum(roleType.getSalut(), attributes.getSalut()));
        character.setMal(sum(roleType.getMal(), attributes.getMal()));
        return repository.save(character);
    }

    private long sum(Long source, Long target) {
        long s = source != null ? source : 0;
        long t = target != null ? target : 0;
        return s + t;
    }

    @Override
    @Transactional
    public void deleteById(String id) {
        if (id==null)
            throw new IllegalArgumentException("Id cannot be null");
        repository.deleteById(id);
    }

    @Override
    public List<Character> findAll() {
        return repository.findAll();
    }

    @Override
    public List<Character> findByRolType(RoleType roleType) {
        return repository.findByRoleType(roleType);
    }

    @Override
    public List<Character> findByIdUser(String id) {
        return repository.findByIdUser(id);
    }
}
