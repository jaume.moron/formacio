package cat.jaumemoron.formacio.davidgame.service;

import cat.jaumemoron.formacio.davidgame.constants.ActionType;
import cat.jaumemoron.formacio.davidgame.domain.Combat;
import cat.jaumemoron.formacio.davidgame.domain.Torn;

public interface CombatEngineService {
    Torn accioResult(Combat combat, ActionType actionTypeRol1, ActionType actionTypeRol2);
    boolean isCombatFinished(Torn torn);
    void endGame(Combat combat);
}
