package cat.jaumemoron.formacio.davidgame.service;

import cat.jaumemoron.formacio.davidgame.domain.User;

import java.util.List;

public interface UsersService {

    User findById(String id);
    User create(String username, String password, String alias);
    User changeAlias(String id, String alias);
    void deleteById(String id);
    List<User> findAll();
    void activate(String id);
    User findByUsername(String username);
}
