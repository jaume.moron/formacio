package cat.jaumemoron.formacio.davidgame.service.impl;

import cat.jaumemoron.formacio.davidgame.constants.ErrorMessages;
import cat.jaumemoron.formacio.davidgame.constants.UserStatus;
import cat.jaumemoron.formacio.davidgame.domain.Character;
import cat.jaumemoron.formacio.davidgame.domain.User;
import cat.jaumemoron.formacio.davidgame.repository.CharactersRepository;
import cat.jaumemoron.formacio.davidgame.repository.UsersRepository;
import cat.jaumemoron.formacio.davidgame.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

import static cat.jaumemoron.formacio.davidgame.constants.ErrorMessages.*;

@Service
public class UsersServiceImpl implements UsersService {

    @Autowired
    private UsersRepository repository;

    @Autowired
    private CharactersRepository charactersRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Override
    public User findById(String id){
        if (id==null)
            throw new IllegalArgumentException(ARGUMENT_EXCEPTION_ID_NULL);
        Optional<User> optional = repository.findById(id);
        return optional.orElse(null);
    }

    @Override
    @Transactional
    public User create(String username, String password, String alias) {
        if (StringUtils.isEmpty(username))
            throw new IllegalArgumentException(ARGUMENT_EXCEPTION_USERNAME_EMPTY);
        if (StringUtils.isEmpty(password))
            throw new IllegalArgumentException(ARGUMENT_EXCEPTION_CONTRA_EMPTY);
        if (StringUtils.isEmpty(alias))
            throw new IllegalArgumentException(ARGUMENT_EXCEPTION_ALIAS_EMPTY);
        User user = new User();
        user.setUsername(username);
        // Xifrem el password abans de desar-lo a la Bdd
        user.setPassword(passwordEncoder.encode(password));
        user.setAlias(alias);
        return repository.save(user);
    }

    @Override
    @Transactional
    public User changeAlias(String id, String alias) {
        if (id==null)
            throw new IllegalArgumentException(ARGUMENT_EXCEPTION_ID_NULL);
        if (StringUtils.isEmpty(alias))
            throw new IllegalArgumentException(ARGUMENT_EXCEPTION_ALIAS_EMPTY);
        Optional<User> user = repository.findById(id);
        if (!user.isPresent())
            throw new IllegalArgumentException(ErrorMessages.USER_DOES_NOT_EXISTS);
        user.get().setAlias(alias);
        return repository.save(user.get());
    }

    @Override
    @Transactional
    public void deleteById(String id) {
        if (id==null)
            throw new IllegalArgumentException(ARGUMENT_EXCEPTION_ID_NULL);

        List<Character> charList = charactersRepository.findByIdUser(id);
        if(!charList.isEmpty()){
            charactersRepository.deleteByIdUser(id);
        }

        repository.deleteById(id);
    }

    @Override
    public List<User> findAll() {
        return repository.findAll();
    }

    @Override
    @Transactional
    public void activate(String id) {
        if (id==null)
            throw new IllegalArgumentException(ARGUMENT_EXCEPTION_ID_NULL);
        Optional<User> user = repository.findById(id);
        if (!user.isPresent())
            throw new IllegalArgumentException(ErrorMessages.USER_DOES_NOT_EXISTS);
        if (UserStatus.ACTIVE.equals(user.get().getStatus()))
            throw new IllegalArgumentException(ErrorMessages.USER_STATUS_IS_ACTIVE);
        user.get().setStatus(UserStatus.ACTIVE);
        repository.save(user.get());
    }

    @Override
    public User findByUsername(String username){
        if (username==null)
            throw new IllegalArgumentException(ARGUMENT_EXCEPTION_USERNAME_EMPTY);
        return repository.findByUsername(username);
    }
}
