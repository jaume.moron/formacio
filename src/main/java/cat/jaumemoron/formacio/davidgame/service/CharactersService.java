package cat.jaumemoron.formacio.davidgame.service;

import cat.jaumemoron.formacio.davidgame.domain.Character;
import cat.jaumemoron.formacio.davidgame.domain.RoleType;
import cat.jaumemoron.formacio.davidgame.domain.User;
import cat.jaumemoron.formacio.davidgame.dto.CharacterAttributesModified;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.List;

public interface CharactersService {

        Character findById(String id);
        Character create(String idUser, RoleType roleType, String name, CharacterAttributesModified attributes);
        void deleteById(String id);
        List<Character> findAll();
        List<Character> findByIdUser(String id);
        List<Character> findByRolType(RoleType roleType);

        }