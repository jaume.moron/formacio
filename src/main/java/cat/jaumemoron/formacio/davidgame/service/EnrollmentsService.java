package cat.jaumemoron.formacio.davidgame.service;

import cat.jaumemoron.formacio.davidgame.domain.Enrollment;

import java.time.LocalDate;
import java.util.List;

public interface EnrollmentsService {

    Enrollment create(String userId);
    void delete(Enrollment enrollment);
    List<Enrollment> findAll();
    Enrollment findByToken(String token);
    List<Enrollment> findByCreationDateLessThan(LocalDate date);
}
