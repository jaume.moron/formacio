package cat.jaumemoron.formacio.davidgame.service;

import cat.jaumemoron.formacio.davidgame.constants.ActionType;
import cat.jaumemoron.formacio.davidgame.domain.Combat;
import cat.jaumemoron.formacio.davidgame.domain.Torn;

import java.util.List;

public interface TornsService {

    Torn findById(String id);
    Torn create(Combat combat, ActionType accioRol1, ActionType accioRol2, boolean accioRol1Succes, boolean accioRol2Succes, Long accioRol1Value, Long accioRol2Value);
    void deleteById(String id);
    List<Torn> findAll();
    List<Torn> findAllByCombat(Combat combat);

}
