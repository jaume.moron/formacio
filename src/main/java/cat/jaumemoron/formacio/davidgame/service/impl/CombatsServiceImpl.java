package cat.jaumemoron.formacio.davidgame.service.impl;

import cat.jaumemoron.formacio.davidgame.constants.CombatStatus;
import cat.jaumemoron.formacio.davidgame.domain.Character;
import cat.jaumemoron.formacio.davidgame.domain.Combat;
import cat.jaumemoron.formacio.davidgame.repository.CombatsRepository;
import cat.jaumemoron.formacio.davidgame.service.CombatsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static cat.jaumemoron.formacio.davidgame.constants.ErrorMessages.*;



@Service
public class CombatsServiceImpl implements CombatsService {



    @Autowired
    private CombatsRepository repository;
    @Override
    public Combat findById(String id) {
        if(id == null){
            throw new IllegalArgumentException(ARGUMENT_EXCEPTION_ID_NULL);
        }
        Optional<Combat> optional = repository.findById(id);
        return optional.orElse(null);
    }

    @Override
    public Combat create(Character character1, Character character2) {
        if (character1 == null)
            throw new IllegalArgumentException(ARGUMENT_EXCEPTION_ROL1_NULL);
        if (character2 == null)
            throw new IllegalArgumentException(ARGUMENT_EXCEPTION_CHAR2_NULL);
        Combat combat = new Combat();
        combat.setCharacter1(character1);
        combat.setCharacter2(character2);
        combat.setSalutInitChar1(character1.getRoleType().getSalut());
        combat.setSalutInitChar2(character2.getRoleType().getSalut());
        combat.setSalutActualChar1(character1.getRoleType().getSalut());
        combat.setSalutActualChar2(character2.getRoleType().getSalut());
        combat.setStatus(CombatStatus.PENDING);
        combat.setWinner(null);
        return repository.save(combat);
    }

    @Override
    public void deleteById(String id) {
        if(id == null){
            throw new IllegalArgumentException(ARGUMENT_EXCEPTION_ID_NULL);
        }
        repository.deleteById(id);
    }

    @Override
    public List<Combat> findAll() {
        return repository.findAll();
    }

    @Override
    public List<Combat> findAllByRol(Character character1, Character character2) {
        if(character1 == null){
            throw new IllegalArgumentException(ARGUMENT_EXCEPTION_ROL1_NULL);
        }
        if(character2 == null){
            throw new IllegalArgumentException(ARGUMENT_EXCEPTION_CHAR2_NULL);
        }

        return repository.findByCharacter1OrCharacter2(character1, character2);
    }

    @Override
    public Combat setCombatStatus(Combat combat, CombatStatus combatStatus) {
        combat.setStatus(combatStatus);
        return repository.save(combat);
    }

    @Override
    public List<Combat> findCombatByStatus(CombatStatus combatStatus) {
        if(combatStatus == null){
            throw new IllegalArgumentException(ARGUMENT_EXCEPTION_INCORRECT_PARAM);
        }
        return repository.findCombatByStatus(combatStatus);
    }
}
