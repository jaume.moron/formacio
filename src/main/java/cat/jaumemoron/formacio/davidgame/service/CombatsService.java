package cat.jaumemoron.formacio.davidgame.service;

import cat.jaumemoron.formacio.davidgame.constants.CombatStatus;
import cat.jaumemoron.formacio.davidgame.domain.Character;
import cat.jaumemoron.formacio.davidgame.domain.Combat;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CombatsService {
    Combat findById(String id);
    Combat create(Character character1, Character character2);
    void deleteById(String id);
    List<Combat> findAll();
    List<Combat> findAllByRol(Character character1, Character character2);
    Combat setCombatStatus(Combat combat, CombatStatus combatStatus);
    List<Combat> findCombatByStatus(@Param("status") CombatStatus combatStatus);
}
