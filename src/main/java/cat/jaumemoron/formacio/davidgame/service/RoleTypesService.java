package cat.jaumemoron.formacio.davidgame.service;

import cat.jaumemoron.formacio.davidgame.domain.RoleType;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.List;

public interface RoleTypesService {

    RoleType findById(String id);

    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    RoleType create(String name, Long attackSucces, Long dodgeSucces, Long castHealSucces, Long interruptSucces, Long salut, Long armadura, Long mal, Long heal);

    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    RoleType update(String id, Long attackSucces, Long dodgeSucces, Long castHealSucces, Long interruptSucces, Long salut, Long armadura, Long mal, Long heal);

    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    void deleteById(String id) throws IllegalArgumentException ;

    List<RoleType> findAll();
}
