package cat.jaumemoron.formacio.davidgame.service.impl;

import cat.jaumemoron.formacio.davidgame.domain.Character;
import cat.jaumemoron.formacio.davidgame.domain.RoleType;
import cat.jaumemoron.formacio.davidgame.repository.CharactersRepository;
import cat.jaumemoron.formacio.davidgame.repository.RolTypesRepository;
import cat.jaumemoron.formacio.davidgame.service.RoleTypesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
public class RoleTypesServiceImpl implements RoleTypesService {

    @Autowired
    RolTypesRepository repository;

    @Autowired
    CharactersRepository charactersRepository;

    @Override
    @Cacheable(value = "roleTypeCache")
    public RoleType findById(String id) {
        if (id==null)
            throw new IllegalArgumentException("Id cannot be null");
        Optional<RoleType> optional = repository.findById(id);
        return optional.orElse(null);
    }

    @Override
    @CacheEvict(cacheNames = "roleTypeCache", allEntries = true)
    public RoleType create(String name, Long attackSucces, Long dodgeSucces, Long castHealSucces, Long interruptSucces, Long salut, Long armadura, Long mal, Long heal) {
        if (StringUtils.isEmpty(name))
            throw new IllegalArgumentException("Role name must be a name");
        if (StringUtils.isEmpty(attackSucces))
            throw new IllegalArgumentException("attackSucces cannot be empty");
        if (StringUtils.isEmpty(dodgeSucces))
            throw new IllegalArgumentException("dodgeSucces cannot be empty");
        if (StringUtils.isEmpty(castHealSucces))
            throw new IllegalArgumentException("castHealSucces cannot be empty");
        if (StringUtils.isEmpty(interruptSucces))
            throw new IllegalArgumentException("interruptSucces cannot be empty");
        if (StringUtils.isEmpty(salut))
            throw new IllegalArgumentException("salut cannot be empty");
        if (StringUtils.isEmpty(armadura))
            throw new IllegalArgumentException("armadura cannot be empty");
        if (StringUtils.isEmpty(mal))
            throw new IllegalArgumentException("mal cannot be empty");
        if (StringUtils.isEmpty(heal))
            throw new IllegalArgumentException("Heal cannot be empty");
        RoleType roleType = new RoleType();
        roleType.setName(name);
        roleType.setAttackSucces(attackSucces);
        roleType.setCounterSucces(dodgeSucces);
        roleType.setCastHealSucces(castHealSucces);
        roleType.setInterruptSucces(interruptSucces);
        roleType.setSalut(salut);
        roleType.setArmadura(armadura);
        roleType.setMal(mal);
        roleType.setHeal(heal);

        return repository.save(roleType);
    }

    @Override
    @CacheEvict(cacheNames = "roleTypeCache", allEntries = true)
    public RoleType update(String id, Long attackSucces, Long dodgeSucces, Long castHealSucces, Long interruptSucces, Long salut, Long armadura, Long mal, Long heal) {
        if (id==null)
            throw new IllegalArgumentException("Id cannot be null");
        if (StringUtils.isEmpty(attackSucces))
            throw new IllegalArgumentException("attackSucces cannot be empty");
        if (StringUtils.isEmpty(dodgeSucces))
            throw new IllegalArgumentException("dodgeSucces cannot be empty");
        if (StringUtils.isEmpty(castHealSucces))
            throw new IllegalArgumentException("castHealSucces cannot be empty");
        if (StringUtils.isEmpty(interruptSucces))
            throw new IllegalArgumentException("interruptSucces cannot be empty");
        if (StringUtils.isEmpty(salut))
            throw new IllegalArgumentException("salut cannot be empty");
        if (StringUtils.isEmpty(armadura))
            throw new IllegalArgumentException("armadura cannot be empty");
        if (StringUtils.isEmpty(mal))
            throw new IllegalArgumentException("mal cannot be empty");
        if (StringUtils.isEmpty(heal))
            throw new IllegalArgumentException("Heal cannot be empty");
        Optional<RoleType> optional = repository.findById(id);
        if(!optional.isPresent())
            throw new IllegalArgumentException("RoleType not found with id " + id);
        RoleType roleType = optional.get();
        roleType.setAttackSucces(attackSucces);
        roleType.setCounterSucces(dodgeSucces);
        roleType.setCastHealSucces(castHealSucces);
        roleType.setInterruptSucces(interruptSucces);
        roleType.setSalut(salut);
        roleType.setArmadura(armadura);
        roleType.setMal(mal);
        roleType.setHeal(heal);

        return repository.save(roleType);
    }

    @Override
    @CacheEvict(cacheNames = "roleTypeCache", allEntries = true)
    @Transactional
    public void deleteById(String id) throws IllegalArgumentException {
        if (id==null)
            throw new IllegalArgumentException("Id cannot be null");

        Optional<RoleType> optional = repository.findById(id);

        if(optional.isPresent()){
            RoleType roleType = optional.get();
            List<Character> listCharsWithRole = charactersRepository.findByRoleType(roleType);
            if(listCharsWithRole.isEmpty()){
                repository.deleteById(id);
            }else{
                throw new IllegalArgumentException ("Can delete a role used by characters");
            }
        }
    }

    @Override
    @Cacheable(value = "roleTypeCache")
    public List<RoleType> findAll() {
        System.out.println("Enter service findAll");
        return repository.findAll();
    }
}
