package cat.jaumemoron.formacio.davidgame.service.impl;

import cat.jaumemoron.formacio.davidgame.constants.ActionType;
import cat.jaumemoron.formacio.davidgame.domain.Combat;
import cat.jaumemoron.formacio.davidgame.domain.Torn;
import cat.jaumemoron.formacio.davidgame.repository.TornsRepository;
import cat.jaumemoron.formacio.davidgame.service.TornsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static cat.jaumemoron.formacio.davidgame.constants.ErrorMessages.ARGUMENT_EXCEPTION_COMBAT_NULL;
import static cat.jaumemoron.formacio.davidgame.constants.ErrorMessages.ARGUMENT_EXCEPTION_ID_NULL;

@Service
public class TornsServiceImpl implements TornsService {

    @Autowired
    TornsRepository repository;

    @Override
    public Torn findById(String id) {
        if(id == null){
            throw new IllegalArgumentException(ARGUMENT_EXCEPTION_ID_NULL);
        }
        Optional<Torn> optional = repository.findById(id);
        return optional.orElse(null);
    }

    @Override
    public Torn create(Combat combat, ActionType accioRol1, ActionType accioRol2, boolean accioRol1Succes, boolean accioRol2Succes, Long accioRol1Value, Long accioRol2Value) {
        if (combat == null){  throw new IllegalArgumentException(ARGUMENT_EXCEPTION_COMBAT_NULL); }
        Torn torn = new Torn();
        torn.setCombat(combat);
        torn.setAccioRol1(accioRol1);
        torn.setAccioRol2(accioRol2);
        torn.setAccioRol1Succes(accioRol1Succes);
        torn.setAccioRol2Succes(accioRol2Succes);
        torn.setAccioRol1Value(accioRol1Value);
        torn.setAccioRol2Value(accioRol2Value);
        return repository.save(torn);
    }

    @Override
    public void deleteById(String id) {
        if (id==null)
            throw new IllegalArgumentException(ARGUMENT_EXCEPTION_ID_NULL);
        repository.deleteById(id);
    }

    @Override
    public List<Torn> findAll() {
        return repository.findAll();
    }

    @Override
    public List<Torn> findAllByCombat(Combat combat) {
        if (combat == null)
            throw new IllegalArgumentException(ARGUMENT_EXCEPTION_COMBAT_NULL);
        return repository.findAllByCombat(combat);
    }
}
