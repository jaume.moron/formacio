package cat.jaumemoron.formacio.davidgame.service.impl;

import cat.jaumemoron.formacio.davidgame.actions.ActionEngine;
import cat.jaumemoron.formacio.davidgame.actions.GenericAction;
import cat.jaumemoron.formacio.davidgame.constants.ActionType;
import cat.jaumemoron.formacio.davidgame.constants.CombatStatus;
import cat.jaumemoron.formacio.davidgame.constants.Winner;
import cat.jaumemoron.formacio.davidgame.domain.Character;
import cat.jaumemoron.formacio.davidgame.domain.Combat;
import cat.jaumemoron.formacio.davidgame.domain.Torn;
import cat.jaumemoron.formacio.davidgame.repository.CombatsRepository;
import cat.jaumemoron.formacio.davidgame.repository.TornsRepository;
import cat.jaumemoron.formacio.davidgame.service.CombatEngineService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static cat.jaumemoron.formacio.davidgame.constants.ErrorMessages.ARGUMENT_EXCEPTION_ACTIONTYPE_NULL;
import static cat.jaumemoron.formacio.davidgame.constants.ErrorMessages.ARGUMENT_EXCEPTION_COMBAT_NULL;

@Service
public class CombatEngineServiceImpl implements CombatEngineService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CombatEngineService.class);

    @Autowired
    TornsRepository tornsRepository;
    @Autowired
    CombatsRepository combatsRepository;

    @Override
    public Torn accioResult(Combat combat, ActionType actionTypeRol1, ActionType actionTypeRol2 ) {
        if (combat == null)
            throw new IllegalArgumentException(ARGUMENT_EXCEPTION_COMBAT_NULL);
        if (actionTypeRol1 == null || actionTypeRol2 == null)
            throw new IllegalArgumentException(ARGUMENT_EXCEPTION_ACTIONTYPE_NULL);
        LOGGER.debug("Creating turn for combat [{}]: Source action is [{}] Target action is [{}]", combat, actionTypeRol1, actionTypeRol2);
        LOGGER.debug("[{}]'s action is [{}]. [{}]'s action is [{}].", combat.getCharacter1(), actionTypeRol1, combat.getCharacter2(), actionTypeRol2);
        // Treballem sempre amb els chars i, a partir d'ells, obtenim les dades que calquin dels seus rol types
        Character char1 = combat.getCharacter1();
        Character char2 = combat.getCharacter2();

        // Obtenim la classe de l'acció que processarà l'acció
        GenericAction actionChar1 = ActionEngine.getAction(actionTypeRol1, char1, char2);
        GenericAction actionChar2 = ActionEngine.getAction(actionTypeRol2, char2, char1);
        // Evaluem les accions
        actionChar1.evaluate();
        actionChar2.evaluate();
        // Obtenim el valor de les accions (mal, curació, etc)
        Long valueChar1 = getActionValue(actionChar1, actionChar2);
        Long valueChar2 = getActionValue(actionChar2, actionChar1);
        // Desem les dades del torn
        Torn torn = new Torn();
        torn.setAccioRol1(actionTypeRol1);
        torn.setAccioRol2(actionTypeRol2);
        torn.setAccioRol1Succes(actionChar1.isSucess());
        torn.setAccioRol2Succes(actionChar2.isSucess());
        torn.setAccioRol1Value(valueChar1);
        torn.setAccioRol2Value(valueChar2);
        torn.setCombat(combat);
        UpdateSalut(torn);
        torn = tornsRepository.save(torn);
        LOGGER.debug("Turn [{}] created successfully", torn);
        return torn;
    }

    private Long getActionValue(GenericAction action1, GenericAction action2){
        LOGGER.debug("Action [{}] of character [{}] evaluated as [{}]", action1, action1.getSource(), action1.isSucess()?"SUCCESS":"FAILED");
        Long value = 0L;
        // Validem si l'acció no ha de ser cancelada per l'acció de l'oponent
        if (!ActionEngine.actionMustBeCancelled(action1, action2)) {
            value = action1.getValue();
            LOGGER.debug("Value of Action [{}] of character [{}] is [{}]", action1, action1.getSource(), value);
        }
        return value;
    }


    @Override
    public boolean isCombatFinished(Torn torn) {
        Combat combat;
        combat = torn.getCombat();

        if (combat.getSalutActualChar1() <= 0 && combat.getSalutActualChar2() <= 0){
            combat.setWinner(Winner.DRAW);
            LOGGER.debug("[{}] game!", Winner.DRAW);
            return true;
        }
        if (combat.getSalutActualChar1() <= 0){
            combat.setWinner(Winner.PLAYER2);
            LOGGER.debug("[{}]-> [{}]  Win!", Winner.PLAYER2, combat.getCharacter2().getName());
            return true;
        }
        if (combat.getSalutActualChar2() <= 0){
            combat.setWinner(Winner.PLAYER1);
            LOGGER.debug("[{}]-> [{}]  Win!", Winner.PLAYER1, combat.getCharacter1().getName());
            return true;
        }

        return false;
    }

    @Override
    public void endGame(Combat combat) {

    }



    private Combat UpdateSalut(Torn torn){
        Combat combat = torn.getCombat();
        if (combat == null)
            throw new IllegalArgumentException(ARGUMENT_EXCEPTION_COMBAT_NULL);
        if(torn.getAccioRol1Succes()){
            if (torn.getAccioRol1() == ActionType.ATTACK || torn.getAccioRol1() == ActionType.COUNTER){
                long salutActual = combat.getSalutActualChar2() - torn.getAccioRol1Value();
                if(salutActual < 0){
                    salutActual = 0L;
                }
                combat.setSalutActualChar2(salutActual);
            }
            if (torn.getAccioRol1() == ActionType.HEAL){
                if(combat.getSalutActualChar1() + torn.getAccioRol1Value() > combat.getSalutInitChar1()){
                    combat.setSalutActualChar1(combat.getSalutInitChar1());
                }else{
                    combat.setSalutActualChar1(combat.getSalutActualChar1() + torn.getAccioRol1Value());
                }
            }
        }
        if(torn.getAccioRol2Succes()){
            if (torn.getAccioRol2() == ActionType.ATTACK || torn.getAccioRol2() == ActionType.COUNTER){
                long salutActual = combat.getSalutActualChar1() - torn.getAccioRol2Value();
                if(salutActual < 0){
                    salutActual = 0L;
                }
                combat.setSalutActualChar1(salutActual);
            }
            if (torn.getAccioRol2() == ActionType.HEAL){
                if(combat.getSalutActualChar2() + torn.getAccioRol2Value() > combat.getSalutInitChar2()){
                    combat.setSalutActualChar2(combat.getSalutInitChar2());
                }else{
                    combat.setSalutActualChar2(combat.getSalutActualChar2() + torn.getAccioRol2Value());
                }
            }
        }
        LOGGER.debug("SALUT [{}] -> [{}] || SALUT [{}] -> [{}]", combat.getCharacter1(), combat.getSalutActualChar1(), combat.getCharacter2(), combat.getSalutActualChar2());
        return combatsRepository.save(combat);
    }

}
