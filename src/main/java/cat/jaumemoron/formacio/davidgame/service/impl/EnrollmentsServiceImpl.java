package cat.jaumemoron.formacio.davidgame.service.impl;

import cat.jaumemoron.formacio.davidgame.domain.Enrollment;
import cat.jaumemoron.formacio.davidgame.repository.EnrollmentsRepository;
import cat.jaumemoron.formacio.davidgame.service.EnrollmentsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.Base64;
import java.util.List;
import java.util.UUID;

import static cat.jaumemoron.formacio.davidgame.constants.ErrorMessages.*;

@Service
public class EnrollmentsServiceImpl implements EnrollmentsService {

    @Autowired
    private EnrollmentsRepository repository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    @Transactional
    public Enrollment create(String userId) {
        if (userId==null)
            throw new IllegalArgumentException(ARGUMENT_EXCEPTION_ID_NULL);
        Enrollment enrollment = new Enrollment();
        enrollment.setUserId(userId);
        enrollment.setToken(getEnrollmentCode());
        enrollment.setCreationDate(LocalDate.now());
        return repository.save(enrollment);
    }

    @Override
    @Transactional
    public void delete(Enrollment enrollment) {
        if (enrollment==null)
            throw new IllegalArgumentException(ENROLLMENT_IS_NULLL);
        repository.delete(enrollment);
    }

    @Override
    public List<Enrollment> findAll() {
        return repository.findAll();
    }

    @Override
    public Enrollment findByToken(String token){
        if (token==null)
            throw new IllegalArgumentException(ENROLLMENT_TOKEN_IS_NULLL);
        return repository.findByToken(token);
    }

    private String getEnrollmentCode(){
        // Obtenim un UUID, el xifrem i el codifiquem en Base 64
        UUID uuid = UUID.randomUUID();
        return new String(Base64.getEncoder().encode(passwordEncoder.encode(uuid.toString()).getBytes()));
    }

    public List<Enrollment> findByCreationDateLessThan(LocalDate date){
        if (date==null)
            throw new IllegalArgumentException(ENROLLMENT_DATE_IS_NULLL);
        return repository.findByCreationDateLessThan(date);
    }
}
