package cat.jaumemoron.formacio.davidgame.actions;

import cat.jaumemoron.formacio.davidgame.constants.ActionType;
import cat.jaumemoron.formacio.davidgame.domain.Character;

public class CounterAction extends GenericAction {

    CounterAction(Character source, Character target) {
        super(source, target, new ActionType[0]);
    }

    // Indiquem el tipus d'acció
    @Override
    public ActionType getType(){
        return ActionType.COUNTER;
    }

    @Override
    public void evaluate() {
        success = genericEvaluate(source.getRoleType().getCounterSucces());
    }

    @Override
    public Long getValue() {
        return Math.round((float)(source.getMal() * 50L) / 100L) - target.getArmadura();
    }
}
