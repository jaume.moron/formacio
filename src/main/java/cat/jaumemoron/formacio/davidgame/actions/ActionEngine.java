package cat.jaumemoron.formacio.davidgame.actions;

import cat.jaumemoron.formacio.davidgame.constants.ActionType;
import cat.jaumemoron.formacio.davidgame.domain.Character;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ActionEngine {

    private static final Logger LOGGER = LoggerFactory.getLogger(ActionEngine.class);

    public static GenericAction getAction(ActionType actionType, Character source, Character target) {
        GenericAction action = null;
        switch (actionType) {
            case ATTACK:
                action = new AttackAction(source, target);
                break;
            case COUNTER:
                action = new CounterAction(source, target);
                break;
            case HEAL:
                action = new HealAction(source, target);
                break;
            case INTERRUPT:
                action = new InterruptAction(source, target);
                break;
        }
        return action;
    }

    public static boolean actionMustBeCancelled(GenericAction source, GenericAction target){
        boolean retval;
        // Si l'acció source no s'ha conseguit retornem sempre false
        if (!source.isSucess())
            retval = true;
        else { // Si l'acció target no s'ha conseguit retornem sempre true
            if (!target.isSucess())
                retval = false;
            else{ // Si les dues accions tenen éxit mirem si la primera cancela per la segona
                retval = source.actionMustBeCancelled(target);
                // SI l'acció no té efectes ho escribim al log
                if (retval)
                    LOGGER.debug("Action [{}] has no effects against action [{}]", source, target);
            }
        }
        return retval;
    }
}
