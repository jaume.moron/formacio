package cat.jaumemoron.formacio.davidgame.actions;

import cat.jaumemoron.formacio.davidgame.constants.ActionType;
import cat.jaumemoron.formacio.davidgame.domain.Character;

public class InterruptAction extends GenericAction {

    InterruptAction(Character source, Character target) {
        super(source, target, new ActionType[0]);
    }

    // Indiquem el tipus d'acció
    @Override
    public ActionType getType(){
        return ActionType.INTERRUPT;
    }

    @Override
    public void evaluate() {
        success = genericEvaluate(source.getRoleType().getInterruptSucces());
    }

    @Override
    public Long getValue() {
        // Aquesta acció no té cap valor
        return null;
    }
}
