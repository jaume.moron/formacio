package cat.jaumemoron.formacio.davidgame.actions;

import cat.jaumemoron.formacio.davidgame.constants.ActionType;
import cat.jaumemoron.formacio.davidgame.domain.Character;

import java.security.SecureRandom;
import java.util.Arrays;
import java.util.List;

public abstract class GenericAction {

    // Primer participant del combat
    protected Character source;
    // Segon del combat
    protected Character target;
    // Indicador de si l'acció ha tingut èxit
    protected boolean success = false;
    // Llista d'accions que cancelen aquesta acció
    protected List<ActionType> cancelableActionTypes;

    protected GenericAction(Character source, Character target, ActionType[] cancelableActionTypes) {
        this.source = source;
        this.target = target;
        this.cancelableActionTypes =  Arrays.asList(cancelableActionTypes);
    }

    // Mëtode per obtenir el tipus d'acció
    public abstract ActionType getType();

    // Mëtode per evaluar l'acció
    public abstract void evaluate();

    // Mëtode per saber l'impacte de l'acció (mal, curació, etc) que té l'acció
    public abstract Long getValue();

    // Métode que indica si l'acció ha anat bé o no
    public boolean isSucess(){
        return success;
    }

    // Mëtode per saber si l'acció es veu cancelada per l'acció de l'oponent
    public boolean actionMustBeCancelled(GenericAction action){
        return cancelableActionTypes.contains(action.getType());
    }

    // Métode genéric per a càlcul d'exit de l'acció
    protected boolean genericEvaluate(Long succes) {
        SecureRandom rand = new SecureRandom(); //instance of random class
        int upperbound = 100;
        int intRandom = rand.nextInt(upperbound) + 1;
        return succes > intRandom;
    }

    @Override
    public String toString() {
        return getType().name();
    }

    public Character getSource() {
        return source;
    }

    public Character getTarget() {
        return target;
    }
}
