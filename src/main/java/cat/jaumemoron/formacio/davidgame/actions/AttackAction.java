package cat.jaumemoron.formacio.davidgame.actions;

import cat.jaumemoron.formacio.davidgame.constants.ActionType;
import cat.jaumemoron.formacio.davidgame.domain.Character;

public class AttackAction extends GenericAction {

    // Constructor privat perque no el pugui fer servir ningú
    AttackAction(Character source, Character target) {
        super(source, target, new ActionType[]{ActionType.COUNTER});
    }

    // Indiquem el tipus d'acció
    @Override
    public ActionType getType(){
        return ActionType.ATTACK;
    }

    @Override
    public void evaluate() {
        // Cridem al métode per obtenir el càlcul genéric. Si volguéssim, que cada acció tinguès un càlcul diferent, el canviarem aquí
        success = genericEvaluate(source.getRoleType().getAttackSucces());
    }

    @Override
    public Long getValue() {
        // Calculem el mal fet restant-lo de l'armadura
        return source.getMal() - target.getArmadura();
    }
}
