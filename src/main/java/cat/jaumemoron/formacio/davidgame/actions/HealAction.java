package cat.jaumemoron.formacio.davidgame.actions;

import cat.jaumemoron.formacio.davidgame.constants.ActionType;
import cat.jaumemoron.formacio.davidgame.domain.Character;

public class HealAction extends GenericAction {

    HealAction(Character source, Character target) {
        super(source, target, new ActionType[]{ActionType.INTERRUPT});
    }

    // Indiquem el tipus d'acció
    @Override
    public ActionType getType(){
        return ActionType.HEAL;
    }

    @Override
    public void evaluate() {
        success = genericEvaluate(source.getRoleType().getCastHealSucces());
    }

    @Override
    public Long getValue() {
        //tornem el valor de la curació
        return source.getHeal();
    }
}
