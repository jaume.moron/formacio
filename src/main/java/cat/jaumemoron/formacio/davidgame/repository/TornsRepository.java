package cat.jaumemoron.formacio.davidgame.repository;

import cat.jaumemoron.formacio.davidgame.domain.Combat;
import cat.jaumemoron.formacio.davidgame.domain.Torn;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TornsRepository extends JpaRepository<Torn, String> {
    List<Torn> findAllByCombat(Combat combat);
}
