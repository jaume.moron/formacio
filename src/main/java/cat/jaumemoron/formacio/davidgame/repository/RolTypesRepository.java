package cat.jaumemoron.formacio.davidgame.repository;

import cat.jaumemoron.formacio.davidgame.domain.RoleType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RolTypesRepository extends JpaRepository<RoleType, String> {
}
