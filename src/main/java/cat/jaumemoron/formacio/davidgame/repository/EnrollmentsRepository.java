package cat.jaumemoron.formacio.davidgame.repository;

import cat.jaumemoron.formacio.davidgame.domain.Enrollment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface EnrollmentsRepository extends JpaRepository<Enrollment, String> {

    Enrollment findByToken(String token);
    List<Enrollment> findByCreationDateLessThan(LocalDate date);
}
