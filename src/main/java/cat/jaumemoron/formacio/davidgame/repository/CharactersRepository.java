package cat.jaumemoron.formacio.davidgame.repository;

import cat.jaumemoron.formacio.davidgame.domain.Character;
import cat.jaumemoron.formacio.davidgame.domain.RoleType;
import cat.jaumemoron.formacio.davidgame.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CharactersRepository extends JpaRepository<Character, String> {
    List<Character> findByRoleType(RoleType roleType);
    List<Character> findByIdUser(String id);
    void deleteByIdUser(String id);
}
