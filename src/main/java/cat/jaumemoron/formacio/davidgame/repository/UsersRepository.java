package cat.jaumemoron.formacio.davidgame.repository;

import cat.jaumemoron.formacio.davidgame.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsersRepository extends JpaRepository<User, String> {

    User findByUsername(String username);
}
