package cat.jaumemoron.formacio.davidgame.repository;

import cat.jaumemoron.formacio.davidgame.constants.CombatStatus;
import cat.jaumemoron.formacio.davidgame.domain.Character;
import cat.jaumemoron.formacio.davidgame.domain.Combat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CombatsRepository extends JpaRepository<Combat, String> {
    List<Combat> findByCharacter1OrCharacter2(Character character1, Character character2);
    List<Combat> findCombatByStatus(@Param("status") CombatStatus combatStatus);
}
