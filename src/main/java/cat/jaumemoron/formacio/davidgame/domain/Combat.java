package cat.jaumemoron.formacio.davidgame.domain;

import cat.jaumemoron.formacio.davidgame.constants.CombatStatus;
import cat.jaumemoron.formacio.davidgame.constants.Winner;

import javax.persistence.*;

@Entity
@Table(name = "COMBATS")
public class Combat extends GenericEntity{

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ID_CHAR_1")
    private Character character1;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ID_CHAR_2")
    private Character character2;

    @Column(name = "SALUT_INIT_CHAR1")
    private Long salutInitChar1;

    @Column(name = "SALUT_INIT_CHAR2")
    private Long salutInitChar2;

    @Column(name = "SALUT_ACTUAL_ROL1")
    private Long salutActualChar1;

    @Column(name = "SALUT_ACTUAL_CHAR2")
    private Long salutActualChar2;

    @Column(name = "STAT")
    private CombatStatus status;

    @Column(name = "WINNER")
    private Winner winner;

    public Character getCharacter1() { return character1; }

    public void setCharacter1(Character character1) { this.character1 = character1; }

    public Character getCharacter2() { return character2; }

    public void setCharacter2(Character character2) { this.character2 = character2; }

    public Long getSalutInitChar1() { return salutInitChar1; }

    public void setSalutInitChar1(Long salutInitRol1) { this.salutInitChar1 = salutInitRol1; }

    public Long getSalutInitChar2() { return salutInitChar2; }

    public void setSalutInitChar2(Long salutInitRol2) { this.salutInitChar2 = salutInitRol2; }

    public Long getSalutActualChar1() { return salutActualChar1; }

    public void setSalutActualChar1(Long salutActualRol1) { this.salutActualChar1 = salutActualRol1; }

    public Long getSalutActualChar2() { return salutActualChar2; }

    public void setSalutActualChar2(Long salutActualRol2) { this.salutActualChar2 = salutActualRol2; }

    public CombatStatus getStatus() { return status; }

    public void setStatus(CombatStatus status) { this.status = status; }

    public Winner getWinner() {
        return winner;
    }

    public void setWinner(Winner winner) {
        this.winner = winner;
    }

    @Override
    public String toString() {
        return getId().toString();
    }
}
