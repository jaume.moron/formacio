package cat.jaumemoron.formacio.davidgame.domain;

import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;

@MappedSuperclass
public abstract class CharacterAttributes extends GenericEntity {

    @Column(name = "SALUT")
    @ApiModelProperty(value = "The total heal", required = true, example = "100")
    private Long salut;

    @Column(name = "ARMADURA")
    @ApiModelProperty(value = "The total armor", required = true, example = "100")
    private Long armadura;

    @Column(name = "MAL")
    @ApiModelProperty(value = "The total damage", required = true, example = "50")
    private Long mal;

    @Column(name = "HEAL")
    @ApiModelProperty(value = "The total heal", required = true, example = "15")
    private Long heal;

    public Long getSalut() {
        return salut;
    }

    public void setSalut(Long salut) {
        this.salut = salut;
    }

    public Long getArmadura() {
        return armadura;
    }

    public void setArmadura(Long armadura) {
        this.armadura = armadura;
    }

    public Long getMal() {
        return mal;
    }

    public void setMal(Long mal) {
        this.mal = mal;
    }

    public Long getHeal() {
        return heal;
    }

    public void setHeal(Long heal) {
        this.heal = heal;
    }
}
