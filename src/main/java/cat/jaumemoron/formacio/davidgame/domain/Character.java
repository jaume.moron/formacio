package cat.jaumemoron.formacio.davidgame.domain;

import javax.persistence.*;

@Entity
@Table(name = "CHARACTERS")
public class Character extends CharacterAttributes{
    @SequenceGenerator(
            name = "CHARACTERS_SEQUENCE_GENERATOR",
            sequenceName = "CHARACTERS_SEQUENCE",
            allocationSize = 1
    )

    @Column(name = "ID_USER")
    private String idUser;

    @Column(name = "NOM")
    private String  name;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ID_CHAR_TYPE")
    private RoleType roleType;

    public String getName() { return this.name; }

    public void setName(String name) { this.name = name;}

    public String getIdUser() { return idUser; }

    public void setIdUser(String idUser) { this.idUser = idUser; }

    public RoleType getRoleType() {
        return roleType;
    }

    public void setRoleType(RoleType roleType) {
        this.roleType = roleType;
    }

    @Override
    public String toString() {
        return name;
    }
}