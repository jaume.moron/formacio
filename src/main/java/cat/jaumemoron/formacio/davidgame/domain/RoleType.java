package cat.jaumemoron.formacio.davidgame.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;

@Entity
@Table(name = "ROLE_TYPES")
@ApiModel(value = "Role")
public class RoleType extends CharacterAttributes {

    @SequenceGenerator(
            name = "ROLE_TYPES_SEQUENCE_GENERATOR",
            sequenceName = "ROLE_TYPES_SEQUENCE",
            allocationSize = 1
    )

    @Column(name = "ROLE_NAME")
    @ApiModelProperty(value = "The role name", required = true, example = "USER")
    private String name;

    @Column(name = "ATTACK_SUCCES")
    @ApiModelProperty(value = "The percent of probability that an attack action will be successfully", required = true, example = "20")
    private Long attackSucces;

    @Column(name = "COUNTER_SUCCES")
    @ApiModelProperty(value = "The percent of probability that a dodge action will be successfully", required = true, example = "20")
    private Long counterSucces;

    @Column(name = "CAST_HEAL_SUCCES")
    @ApiModelProperty(value = "The percent of probability that a heal action will be successfully", required = true, example = "20")
    private Long castHealSucces;

    @Column(name = "INTERRUPT_SUCCES")
    @ApiModelProperty(value = "The percent of probability that an interrupt action will be successfully", required = true, example = "20")
    private Long interruptSucces;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getAttackSucces() {
        return attackSucces;
    }

    public void setAttackSucces(Long attackSucces) {
        this.attackSucces = attackSucces;
    }

    public Long getCounterSucces() {
        return counterSucces;
    }

    public void setCounterSucces(Long counterSucces) {
        this.counterSucces = counterSucces;
    }

    public Long getCastHealSucces() {
        return castHealSucces;
    }

    public void setCastHealSucces(Long castHealSucces) {
        this.castHealSucces = castHealSucces;
    }

    public Long getInterruptSucces() {
        return interruptSucces;
    }

    public void setInterruptSucces(Long interruptSucces) {
        this.interruptSucces = interruptSucces;
    }

    @Override
    public String toString() {
        return name;
    }
}
