package cat.jaumemoron.formacio.davidgame.domain;

import cat.jaumemoron.formacio.davidgame.constants.SecurityConstants;
import cat.jaumemoron.formacio.davidgame.constants.UserStatus;

import javax.persistence.*;

@Entity
@Table(name = "USERS")
public class User extends GenericEntity {

    @Column(name = "USERNAME")
    private String username;

    @Column(name = "PASS")
    private String password;

    @Column(name = "ALIAS")
    private String alias;

    @Column(name = "STAT")
    private UserStatus status = UserStatus.PENDING_OF_ENROLLMENT;

    @Column(name = "ROLE")
    private String role = SecurityConstants.USER;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public UserStatus getStatus() {
        return status;
    }

    public void setStatus(UserStatus status) {
        this.status = status;
    }

    public String getRole() {
        return role;
    }
}
