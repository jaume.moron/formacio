package cat.jaumemoron.formacio.davidgame.domain;

import cat.jaumemoron.formacio.davidgame.constants.ActionType;

import javax.persistence.*;

@Entity
@Table(name = "TORNS")
public class Torn extends GenericEntity{

    @Column(name = "ACCIO_ROL1")
    private ActionType accioRol1;

    @Column(name = "ACCIO_ROL1_SUCCES")
    private Boolean accioRol1Succes;

    @Column(name = "ACCIO_ROL1_VALUE")
    private Long accioRol1Value;

    @Column(name = "ACCIO_CHAR2")
    private ActionType accioRol2;

    @Column(name = "ACCIO_CHAR2_SUCCES")
    private Boolean accioRol2Succes;

    @Column(name = "ACCIO_CHAR2_VALUE")
    private Long accioRol2Value;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ID_COMBAT")
    private Combat combat;

    public ActionType getAccioRol1() { return accioRol1; }

    public void setAccioRol1(ActionType accioRol1) { this.accioRol1 = accioRol1; }

    public ActionType getAccioRol2() { return accioRol2; }

    public void setAccioRol2(ActionType accioRol2) { this.accioRol2 = accioRol2; }

    public Boolean getAccioRol1Succes() { return accioRol1Succes; }

    public void setAccioRol1Succes(Boolean accioRol1Succes) { this.accioRol1Succes = accioRol1Succes; }

    public Long getAccioRol1Value() { return accioRol1Value; }

    public void setAccioRol1Value(Long accioRol1Value) { this.accioRol1Value = accioRol1Value; }

    public Long getAccioRol2Value() { return accioRol2Value; }

    public void setAccioRol2Value(Long accioRol2Value) { this.accioRol2Value = accioRol2Value; }

    public Boolean getAccioRol2Succes() { return accioRol2Succes; }

    public void setAccioRol2Succes(Boolean accioRol2Succes) { this.accioRol2Succes = accioRol2Succes; }

    public Combat getCombat() { return combat; }

    public void setCombat(Combat combat) { this.combat = combat; }

    @Override
    public String toString() {
        return getId() + " - Combat id: " + combat.getId();
    }
}