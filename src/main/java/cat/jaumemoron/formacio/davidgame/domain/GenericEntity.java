package cat.jaumemoron.formacio.davidgame.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.UUID;

@MappedSuperclass
public class GenericEntity implements Serializable {

    @Id
    @Column(name = "ID")
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @PrePersist
    public void handlePrePersist(){
        // Assignem un id
        this.id = UUID.randomUUID().toString();
    }
}
