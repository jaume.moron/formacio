package cat.jaumemoron.formacio.davidgame.domain;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "ENROLLMENTS")
public class Enrollment extends GenericEntity {
    @SequenceGenerator(
            name = "ENROLLMENTS_SEQUENCE_GENERATOR",
            sequenceName = "ENROLLMENTS_SEQUENCE",
            allocationSize = 1
    )

    @Column(name = "ID_USER")
    private String userId;

    @Column(name = "TOKEN")
    private String token;

    @Column(name = "CREATION_DATE")
    private LocalDate creationDate;

    public String getUserId() {
        return userId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String code) {
        this.token = code;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }
}