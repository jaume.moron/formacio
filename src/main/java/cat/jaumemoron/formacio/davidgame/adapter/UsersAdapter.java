package cat.jaumemoron.formacio.davidgame.adapter;

import cat.jaumemoron.formacio.davidgame.constants.ErrorMessages;
import cat.jaumemoron.formacio.davidgame.domain.Enrollment;
import cat.jaumemoron.formacio.davidgame.domain.User;
import cat.jaumemoron.formacio.davidgame.exceptions.UserExistsException;
import cat.jaumemoron.formacio.davidgame.service.EnrollmentsService;
import cat.jaumemoron.formacio.davidgame.service.UsersService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;

import static cat.jaumemoron.formacio.davidgame.constants.ErrorMessages.USER_ALREADY_EXISTS_PARAM_USERNAME;

@Component
public class UsersAdapter {

    private static final Logger LOGGER = LoggerFactory.getLogger(UsersAdapter.class);

    @Autowired
    private UsersService usersService;

    @Autowired
    private EnrollmentsService enrollmentsService;

    @Autowired
    private JavaMailSender javaMailSender;

    @Value("${enrollments.daysToLive}")
    private int daysToLive;

    @Value("${mailing.sendMail}")
    private boolean sendMail;

    @Value("${mailing.serverMail}")
    private String serverMail;

    @Transactional
    public void userRegistration(String username, String password, String alias, Model model) throws UserExistsException {
        //Comprobem que no existeix
        User userRecovered = usersService.findByUsername(username);
        /*
          Jaume: 2)
          Al no tenir haver definit comportament al mock per al servei findByUsername, retornarà un null, és a dir
          com si no existís l'usuari
        */
        if (userRecovered != null)
            throw new UserExistsException(String.format(USER_ALREADY_EXISTS_PARAM_USERNAME, username));
        // Creem l'usuari
        User user = usersService.create(username, password, alias);
        // Creem l'enrollment de l'usuari
        /* Jaume 3:
           Com al test no hem definit el mock pel enrollmentsService.create, això ens retorna un objecte null
         */
        Enrollment enrollment = enrollmentsService.create(user.getId());
        // Enviem mail a usuari indicant enllaç per finalitzar el registre
        SimpleMailMessage msg = new SimpleMailMessage();
        msg.setTo(user.getUsername());
        msg.setFrom("admin@davidgame.cat");
        msg.setSubject("Fi de registre a DavidGame");
        /* Jaume 4:
           Al métode de sota li estem passant l'objecte user (en el test hem definit el comportament del mock) i el enrollment (a null)
         */
        msg.setText(getEmailText(user, enrollment));
        if(sendMail){
            javaMailSender.send(msg);
        }else{
            LOGGER.debug("Token generated is [{}]", enrollment.getToken());
        }


    }

    @Transactional
    public void endUserRegistration(String token) {
        if (token == null)
            throw new IllegalArgumentException(ErrorMessages.ENROLLMENT_TOKEN_IS_NULLL);
        // Obtenim l'enrollment del token
        Enrollment enrollment = enrollmentsService.findByToken(token);
        if (enrollment == null)
            throw new IllegalArgumentException(ErrorMessages.ENROLLMENT_DOES_NOT_EXISTS);
        // Actualitzem l'estat de l'usuari a activat
        usersService.activate(enrollment.getUserId());
        // Eliminem l'enrollment
        enrollmentsService.delete(enrollment);
    }

    @Scheduled(cron = "${enrollments.deprecation.cron}")
    @Transactional
    public void removeOldEnrollments() {
        List<Enrollment> list = enrollmentsService.findByCreationDateLessThan(LocalDate.now());
        // Per cada enrollment cadacuat, l'elliminem i eliminem l'usuari associat
        for (Enrollment enrollment : list) {
            LOGGER.info("Attempting to remove enrollment [{}] ", enrollment.getId());
            enrollmentsService.delete(enrollment);
            LOGGER.info("Attempting to remove user [{}] ", enrollment.getUserId());
            usersService.deleteById(enrollment.getUserId());
            LOGGER.info("Enrollment [{}] removed successfully", enrollment.getId());
        }
    }

    private String getEmailText(User user, Enrollment enrollment) {
        /* Jaume 5:
           A sota estem fent "enrollment.getToken()" i l'objecte enrollment és null, per tant, normal que salti l'excepció
         */
        return "Hola " + user.getAlias() + "\n" +
                "Prem aquest <a href='" + serverMail + enrollment.getToken() + "'>enllaç</a> per finalitzar el registre\n" +
                "Salutacions";
    }
}
