package cat.jaumemoron.formacio.davidgame.exceptions;

public class UserExistsException extends Exception {

    public UserExistsException(String msg) {
        super(msg);
    }
}
