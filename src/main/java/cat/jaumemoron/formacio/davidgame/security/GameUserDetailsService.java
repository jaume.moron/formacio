package cat.jaumemoron.formacio.davidgame.security;

import cat.jaumemoron.formacio.davidgame.domain.User;
import cat.jaumemoron.formacio.davidgame.repository.UsersRepository;
import cat.jaumemoron.formacio.davidgame.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class GameUserDetailsService  implements UserDetailsService {

    @Autowired
    private UsersService service;

    @Override
    public UserDetails loadUserByUsername(String username) {
        User user = service.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException(username);
        }
        return new UserPrincipal(user);
    }
}