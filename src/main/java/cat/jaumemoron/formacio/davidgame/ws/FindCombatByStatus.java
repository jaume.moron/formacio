package cat.jaumemoron.formacio.davidgame.ws;

import cat.jaumemoron.formacio.davidgame.constants.CombatStatus;
import cat.jaumemoron.formacio.davidgame.ws.impl.FindCombatByStatusImpl;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public interface FindCombatByStatus {
    @WebMethod
    public String findCombatByStatus(String combatStatus);
}
