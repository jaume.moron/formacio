package cat.jaumemoron.formacio.davidgame.ws.impl;

import cat.jaumemoron.formacio.davidgame.service.CombatsService;
import cat.jaumemoron.formacio.davidgame.ws.FindCombatByStatus;
import org.springframework.beans.factory.annotation.Autowired;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService(endpointInterface = "cat.jaumemoron.formacio.davidgame.ws.FindCombatByStatus")
public class FindCombatByStatusImpl implements FindCombatByStatus {

    @Autowired
    CombatsService combatsService;

    @WebMethod
    @Override
    public String findCombatByStatus(String combatStatus) {
        String message = null;

        switch (combatStatus) {
            case "IN_PROCESS":
                message = "Combats in process: ";// + returnMissage(CombatStatus.IN_PROCESS);
                break;
            case "PENDING":
                message = "Combats in pending: ";// + returnMissage(CombatStatus.PENDING);
                break;
            case "FINISHED":
                message = "Combats in finished: ";// + returnMissage(CombatStatus.FINISHED);
                break;
            default:
                message = "No status found ";
        }



        return message;
    }
/*
    private String returnMissage(CombatStatus combatStatus){

        StringBuilder sb = new StringBuilder();
        sb.append("Id's -> ");
        List<Combat> allCombats = combatsService.findCombatByStatus(combatStatus);
        for(int i = 0; i < allCombats.size(); i++) {
            sb.append(allCombats.get(i).getId()+" - ");
        }
        return sb.toString();
    }
 */
}
