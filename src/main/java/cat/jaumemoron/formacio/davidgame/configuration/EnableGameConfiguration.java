package cat.jaumemoron.formacio.davidgame.configuration;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Import({GameConfiguration.class})
@Documented
public @interface EnableGameConfiguration {
}
