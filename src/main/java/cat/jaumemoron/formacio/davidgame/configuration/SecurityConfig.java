package cat.jaumemoron.formacio.davidgame.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .authorizeRequests()
                // Indiquem les url que són visibles per tothom
                // Recursos estàtics
                .antMatchers("/resources/**").permitAll()
                .antMatchers("/webjars/**").permitAll()
                .antMatchers("/css/**").permitAll()
                .antMatchers("/js/**").permitAll()
                .antMatchers("/img/**").permitAll()
                .antMatchers("/icon-fonts/**").permitAll()
                // Obrim els recursos de Swagger (només per les nostres proves de la API)
                .antMatchers("/swagger-ui.html").permitAll()
                .antMatchers("/swagger*/**").permitAll()
                .antMatchers("/v2/api-docs").permitAll()
                // Accions de registre d'usuaris
                .antMatchers("/register/**").permitAll()
                // Definim les url depenents del seu rol
                .antMatchers("/api/**").permitAll()
                // Qualsevol altre url, l'usuari ha d'estar autenticat
                // Obro els presimisos per els personatges i rols per fer proves
                .antMatchers("/characters/**").authenticated()
                .antMatchers("/roles/**").authenticated()
                .antMatchers("/combats/**").authenticated()
                .antMatchers("/actuator/**").permitAll()
                .anyRequest().permitAll()
                .and()
                // Definim la pàgina de login
                .formLogin()
                .loginPage("/login")
                .usernameParameter("username").passwordParameter("password")
                // TODO: Posar la url on l'usuari pot administrar ninos i fer combats
                .defaultSuccessUrl("/")
                .permitAll()
                .and()
                // Definim el logout
                .logout()
                .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                .permitAll()
                .logoutSuccessUrl("/");
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder);
    }

}
