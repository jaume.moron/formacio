package cat.jaumemoron.formacio.davidgame;

import cat.jaumemoron.formacio.davidgame.configuration.EnableGameConfiguration;
import cat.jaumemoron.formacio.davidgame.ws.impl.FindCombatByStatusImpl;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

import javax.xml.ws.Endpoint;

@SpringBootApplication
@EnableGameConfiguration
@ServletComponentScan
public class DavidGameApplication {

    public static void main(String[] args) {
        Endpoint.publish("http://localhost:7070/WS/FindCombatByStatus", new FindCombatByStatusImpl());
        SpringApplication.run(DavidGameApplication.class, args);
    }

}
