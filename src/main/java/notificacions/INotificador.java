package notificacions;

public interface INotificador {
    // Métode per obtenir el nom del notificador
    String getName();

    // Métodoe que indica si el notificador és actiu
    boolean enabled();

    // Métode que envia la notificació
    boolean notifica(Missatge missatge);
}
