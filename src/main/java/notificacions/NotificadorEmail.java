package notificacions;

public class NotificadorEmail implements INotificador {

    @Override
    public String getName() {
        return "EMAIL";
    }

    @Override
    public boolean enabled() {
        // Harcodegem true però la la idea es obtenir el resultat, per exemple, d'una connexió a un servidor
        // per saber si es poden enviar notificacions en aquest moment o no
        return true;
    }

    @Override
    public boolean notifica(Missatge missatge) {
        System.out.println("S'envia el missatge per EMAIL: " + missatge);
        return true;
    }
}
