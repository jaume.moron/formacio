package notificacions;

public class NotificadorService {

    private INotificador notificador;

    public NotificadorService(INotificador notificador) {
        this.notificador = notificador;
    }

    public boolean sendMessage(String assumpte, String cos){
        boolean retval= false;
        Missatge missatge = new Missatge(assumpte, cos);
        if (notificador.enabled()) {
            System.out.println("Missatge [" + missatge + "] enviant-se pel notificador [" + notificador.getName() + "]");
            return notificador.notifica(missatge);
        }else
            throw new IllegalArgumentException("El notificador [" + notificador.getName() + "] no pot enviar missatges");

    }
}
