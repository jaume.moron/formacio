package notificacions;

public class Missatge {

    private String assumpte;
    private String cos;

    public Missatge(String assumpte, String cos) {
        this.assumpte = assumpte;
        this.cos = cos;
    }

    public String getAssumpte() {
        return assumpte;
    }

    public String getCos() {
        return cos;
    }

    @Override
    public String toString() {
        return assumpte + "-" + cos;
    }
}
