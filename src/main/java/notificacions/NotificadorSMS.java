package notificacions;

public class NotificadorSMS implements INotificador {

    @Override
    public String getName() {
        return "SMS";
    }

    @Override
    public boolean enabled() {
        // Harcodegem false però la la idea es obtenir el resultat, per exemple, d'una connexió a un servidor
        // per saber si es poden enviar notificacions en aquest moment o no
        return false;
    }

    @Override
    public boolean notifica(Missatge missatge) {
        System.out.println("S'envia el missatge per SMS: " + missatge);
        return false;
    }
}
