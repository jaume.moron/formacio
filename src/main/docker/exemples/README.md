# Mini tutorial docker

Si no tenim docker instal·lat en local podem fer proves a la següent web:

    https://labs.play-with-docker.com/

## 1 Instal·lar portainer

Executar la següent comanda depenent del sistema operatiu del host

#### Linux
    docker run -d -p 9000:9000 -v /var/run/docker.sock:/var/run/docker.sock portainer/portainer

#### Windows

    docker run -d -p 9000:9000 --name portainer --restart always -v \\.\pipe\docker_engine:\\.\pipe\docker_engine portainer/portainer  

Executar la següent comanda per veure els contenidors aixecats

    docker ps

Executar la següent comanda per veure tots els contenidors creats

    docker ps -all
    
### 1.1 Configuració

Accedir a localhost:9000

Indicar usuari i password:

![user $ password](https://clouding.io/hc/article_attachments/360006174739/KB-Portainer-02.png)

Escollir a quin entorn de docker es vol connectar, en el nostre cas, al local

![Docker environment](https://clouding.io/hc/article_attachments/360006174719/KB-Portainer-03.png)

### 1.2 Conceptes

* Containers: Llista de tots els contenidors
* Images: Imatges de dockers descarregades
* Stacks: Dockers de cada docker-compose executat
* Networks: Xarxes virtuals creades (cada docker-compose té mínim una)
* Volumes: Volums interns dels contenidors 

## 2. Docker-compose

### 2.1 Wordpress sense volums

Crear directori nou i copiar el següent fitxer dins

* [docker-compose.yml](./1_wordpress/docker-compose.yml)

Executar la següent comanda per iniciar els contenidors

    docker-compose up -d
   
Accedir a localhost:8000 i configurar Wordpress

Executar la següent comanda per eliminar els contenidors

    docker-compose down

Executar la següent comanda per veure que els contenidors s'han eliminat

    docker ps -all
    
Executar la següent comanda per iniciar els contenidors de nou

    docker-compose up -d
    
Al accedir a localhost:8000 ens demanarà configurar wordpress de nou

### 2.2 Wordpress amb volums

Crear directori nou i copiar el següent fitxer dins

* [docker-compose.yml](./2_wordpress-volume/docker-compose.yml)

Executar la següent comanda per iniciar els contenidors

    docker-compose up -d

Accedir a localhost:8001 i configurar Wordpress

Executar la següent comanda per eliminar els contenidors

    docker-compose down

Executar la següent comanda per veure que els contenidors s'han eliminat

    docker ps -all
    
Executar la següent comanda per iniciar els contenidors de nou

    docker-compose up -d
    
Al accedir a localhost:8000 no caldrà configurar de nou perque les dades de la bdd les agafa del volum creat


### 2.3 Comunicació entre volums

A portainer, connectar-nos via consola a wordpress1 i executar la següent comanda:

    curl db:3306
    
Es mostrarà el següent missatge:

    Warning: Binary output can mess up your terminal. Use "--output -" to tell 
    Warning: curl to output it to your terminal anyway, or consider "--output 
    Warning: <FILE>" to save to a file.

Això és perque arriba al host però no pot processar la resposta (és el port de comms de mysql)

Si intentem connectar-nos al mysql de l'altre stack:

    curl db2:3306
    
Es mostrarà el següent missatge:

    curl: (6) Could not resolve host: db2

Això és perque els contenidors d'un i altre stack no tenen visió entre ells.

### 2.4 Monitorització Bdd mysql

Crear directori nou i copiar els següents fitxer dins

* [docker-compose.yml](./2_wordpress-volume/docker-compose.yml)
* [docker-compose.yml](./3_monitoring/docker-compose.yml)
* [prometheus.yml](./3_monitoring/prometheus.yml)

Executar la següent comanda per iniciar els contenidors

    docker-compose up -d

Comprobem que wordpress ha arrencat bé:

    http://localhost:8002/targets
    
Comprobem que prometheus pot accedir al mysqld-exporter:

    http://localhost:9090/targets

Ens loguem a grafana amb l'usuari admin

    http://localhost:3000
    
Configurem el datasource de prometheus indicant la següent url:

    http://prometheus:9090
    
Importem a un nou dashboard escollint un dels següents:
    
    https://grafana.com/grafana/dashboards/6239
    https://grafana.com/grafana/dashboards/11323
    https://grafana.com/grafana/dashboards/7362

## 3 Comandes bàsiques de docker

    docker run -d

    docker ps
    
    docker stats <container_name | Container id>
    
    docker logs <container_name | Container id>
    
    docker logs <container_name | Container id> -f  --> tail -f
    
    docker stop <container_name | Container id>
    
    docker start <container_name | Container id>
    
    docker restart <container_name | Container id>
    
    docker images
    
    docker version
