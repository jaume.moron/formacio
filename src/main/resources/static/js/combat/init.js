let dropdown = $('#charsDropDown');
dropdown.empty();
dropdown.append('<option selected="true" disabled>Choose Character</option>');
dropdown.prop('selectedIndex', 1);



$(document).ready(function()
{
    $.ajax({
        type: "GET",
        dataType: "json",
        url: "/api/character/user",
        success: function(data){
            $.each(data, function (key, entry) {
                dropdown.append($('<option></option>').attr('value', entry.id).text(entry.name));
            })
        }
    });
});
