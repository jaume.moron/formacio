$(".healthPlayer-bar-text").html("100%");
$(".healthPlayer-bar").css({
  "width": "100%"
});
$(".healthComputer-bar-text").html("100%");
$(".healthComputer-bar").css({
  "width": "100%"
});

function applyChange(bar, curHealth, maxHealth) {
    var a = curHealth * (100 / maxHealth);
    if(bar=="fighter"){
        $(".healthPlayer-bar-text").html(Math.round(a) + "%");
        $(".healthPlayer-bar-red").animate({
            'width': a + "%"
        }, 700);
        $(".healthPlayer-bar").animate({
            'width': a + "%"
        }, 500);
        $(".healthPlayer-bar-blue").animate({
            'width': a + "%"
        }, 300);
    }else{
        $(".healthComputer-bar-text").html(Math.round(a) + "%");
        $(".healthComputer-bar-red").animate({
            'width': a + "%"
          }, 700);
        $(".healthComputer-bar").animate({
            'width': a + "%"
        }, 500);
        $(".healthComputer-bar-blue").animate({
            'width': a + "%"
          }, 300);
  }
  $('.total').html(curHealth + "/" + maxHealth);
}

function restart() {
  //Was going to have a game over/restart function here.
  $('.healthPlayer-bar-red, .healthPlayer-bar');
}