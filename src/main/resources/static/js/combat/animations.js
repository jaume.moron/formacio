function attackAnim(accioValue, character, attack, actualHealth, initHealth){
    imgSrc = character.attr('src');
    function complete() {
        swapCharacterImg(character, imgSrc);
    }
    swapCharacterImg(character, attack);
    updateHealthBar(character, actualHealth, initHealth, false);


    Swal.fire({
        title: '<span style="color:#FFF">'+accioValue+'<span>',
        hideClass: {
            popup: 'animate__animated animate__fadeOutUp'
        },
        width: 168,
        padding: 59,
        background: 'transparent url('+attackIconSrc+')',
        onClose: () => {
              character.fadeOut( FADE_CHAR, complete );
              flipImage($('#vs'));
              if(character.attr('id') == 'computer'){
                enableActionButtons();
                checkGameStatus();
              }
        },
        showConfirmButton: false,
        timer: 500,

        backdrop: `
            rgba(123,0,0,0.4)
        `
    })
}

function healAnim(accioValue, character, heal, interrupt, actualHealth, initHealth){

    imgSrc = character.attr('src');

    if(accioValue>0){//not interrupted
        swapCharacterImg(character, heal);
    }
    else{
        swapCharacterImg(character, interrupt);
    }

    updateHealthBar(character, actualHealth, initHealth, true);

    function complete() {
        swapCharacterImg(character, imgSrc);
    }

    Swal.fire({
        title: '<span style="color:#FFF">'+accioValue+'<span>',
        hideClass: {
            popup: 'animate__animated animate__fadeOutUp'
        },
        width: 168,
        padding: 65,
        background: 'transparent url('+healIconSrc+')',
        onClose: () => {
              character.fadeOut( FADE_CHAR, complete );
              flipImage($('#vs'));
              if(character.attr('id') == 'computer'){
                enableActionButtons();
              }
        },
        showConfirmButton: false,
        timer: 500,

        backdrop: `
            rgba(0,123,0,0.4)
        `
    })

}

function failAnim(character){
    Swal.fire({
        title: "Fail",
        hideClass: {
            popup: 'animate__animated animate__fadeOutUp'
        },
        onClose: () => {
            character.fadeIn(FADE_CHAR);
            flipImage($('#vs'));
            if(character.attr('id') == 'computer'){
                enableActionButtons();
                checkGameStatus();
            }
        },
        showConfirmButton: false,
        timer: 1000,
        backdrop: `
            rgba(123,123,123,0.4)
        `
    })
}

function interruptAnim(character){
    Swal.fire({
        title: "Interrupt",
        hideClass: {
            popup: 'animate__animated animate__fadeOutUp'
        },
        onClose: () => {
            character.fadeIn(FADE_CHAR);
            flipImage($('#vs'));
            if(character.attr('id') == 'computer'){
                enableActionButtons();
                checkGameStatus();
            }
        },
        showConfirmButton: false,
        timer: 1000,
        backdrop: `
            rgba(123,0,123,0.4)
        `
    })
}

function counterAttackAnim(character){

    Swal.fire({
        title: "counterAttack",
        hideClass: {
            popup: 'animate__animated animate__fadeOutUp'
        },
        onClose: () => {
            character.fadeIn(FADE_CHAR);
            flipImage($('#vs'));
            if(character.attr('id') == 'computer'){
                enableActionButtons();
                checkGameStatus();
            }
        },
        showConfirmButton: false,
        timer: 1000,
        backdrop: `
            rgba(123,123,123,0.4)
        `
    })
}

function counteredAnim(character, actualHealth, initHealth){
    updateHealthBar(character, actualHealth, initHealth, true);
    Swal.fire({
        title: "COUNTERED!!",
        hideClass: {
            popup: 'animate__animated animate__fadeOutUp'
        },
        onClose: () => {
            character.fadeIn(FADE_CHAR);
            flipImage($('#vs'));
            if(character.attr('id') == 'computer'){
                enableActionButtons();
                checkGameStatus();
            }
        },
        showConfirmButton: false,
        timer: 1000,
        backdrop: `
            rgba(123,123,123,0.4)
        `
    })
}

