$(document).ready(function()
{
    combat_id=$('#combat_id').val();
    $('#fighterAttack').click(function() {
        disableActionButtons();
        url = "/api/combatEngine?actionFighter=1&id_combat="+combat_id;
        sendAction("ATTACK", url);
    });
    $('#fighterCounter').click(function() {
        disableActionButtons();
        url = "/api/combatEngine?actionFighter=2&id_combat="+combat_id;
        sendAction("COUNTER", url);
    });

    $('#fighterHeal').click(function() {
        disableActionButtons();
        url = "/api/combatEngine?actionFighter=3&id_combat="+combat_id;
        sendAction("HEAL", url);
    });
    $('#fighterInterrupt').click(function() {
        disableActionButtons();
        url = "/api/combatEngine?actionFighter=4&id_combat="+combat_id;
        sendAction("INTERRUPT", url);
    });
    $('#skipAnimations').click(function() {
        skipAnimations();
    });

});

function sendAction(actionType, url){
    $.ajax({
        url: url,
        method: 'GET',
        dataType: 'JSON'
    })
    .then(function (_data) {
        DATA=_data;
        initAnimation(actionType);
    })
    .fail(function (err) {
        Swal.fire({
            icon: 'error',
            title: 'Ooops!!',
            text: err.errors,
            footer: err.status
        })
    });
}

function initAnimation(actionType){
    switch(actionType) {
        case "ATTACK":
            fighterAttack();
            break;
        case "COUNTER":
            fighterCounterAttack();
        break;
        case "HEAL":
            fighterHeal();
            break;
        case "INTERRUPT":
            fighterInterrupt();
            break;
        default:
        //no anim
    }
    setTimeout(computerInitAnimation, 4000);


}
function fighterAttack(){
    function complete() {
        //mirem si s'excuta un counter
        if(DATA.accioRol2Succes && DATA.accioRol2 == "COUNTER"){
            counteredAnim(FIGHTER, DATA.combat.salutActualChar1, DATA.combat.salutInitChar1);
        }else{
            if(DATA.accioRol1Succes){
                attackAnim(DATA.accioRol1Value, FIGHTER, attackSrc, DATA.combat.salutActualChar2, DATA.combat.salutInitChar2);
            }else{
                failAnim(FIGHTER);
            }
        }
    }
    FIGHTER.fadeOut( FADE_CHAR, complete );
}
function fighterHeal(){
        function complete() {
            if(DATA.accioRol1Succes){
                healAnim(DATA.accioRol1Value, FIGHTER, healSrc, interruptedFighterSrc, DATA.combat.salutActualChar1, DATA.combat.salutInitChar1);
            }else{
                failAnim(FIGHTER);
            }
        }
        FIGHTER.fadeOut( FADE_CHAR, complete );
}
function fighterInterrupt(){
        function complete() {
            if(DATA.accioRol1Succes){
                interruptAnim(FIGHTER);
            }else{
                failAnim(FIGHTER);
            }
        }
        FIGHTER.fadeOut( FADE_CHAR, complete );
}
function fighterCounterAttack(){
        function complete() {
            if(DATA.accioRol1Succes){
                counterAttackAnim(FIGHTER);
            }else{
                failAnim(FIGHTER);
            }
        }
        FIGHTER.fadeOut( FADE_CHAR, complete );
}


function computerInitAnimation(){

    function complete() {
        if(DATA.accioRol2Succes){
            switch(DATA.accioRol2) {
                  case "ATTACK":
                    if(DATA.accioRol1Succes && DATA.accioRol1 == "COUNTER"){
                        counteredAnim(COMPUTER, DATA.combat.salutActualChar2, DATA.combat.salutInitChar2);
                    }else{
                        attackAnim(DATA.accioRol2Value, COMPUTER, attackComputerSrc, DATA.combat.salutActualChar1, DATA.combat.salutInitChar1);
                    }
                    break;
                  case "COUNTER"://canviar al servidor la accio de dodge per counterAttack
                    counterAttackAnim(COMPUTER);
                    break;
                  case "HEAL":
                    healAnim(DATA.accioRol2Value, COMPUTER, healComputerSrc, interruptedComputerSrc, DATA.combat.salutActualChar2, DATA.combat.salutInitChar2);
                    break;
                  case "INTERRUPT":
                    interruptAnim(COMPUTER);
                    break;
                  default:
            }
        }else{
            failAnim(COMPUTER);
        }
        updateHealthBar(FIGHTER, DATA.combat.salutActualChar1, DATA.combat.salutInitChar1, true);
        updateHealthBar(COMPUTER, DATA.combat.salutActualChar2, DATA.combat.salutInitChar2, true);
    }
    COMPUTER.fadeOut( FADE_CHAR, complete );
}

function swapCharacterImg(character, src){
    character.attr('src',src).fadeIn(FADE_SWAP);
}

function flipImage(img){
    img.fadeOut(FADE_SWAP, complete);

    function complete() {
        if(img.hasClass('flip')){
            img.removeClass('flip');
        }else{
            img.addClass("flip");
        }
        img.fadeIn(FADE_SWAP);
        img.fadeIn(FADE_SWAP);
    }
}
function checkGameStatus(){
    if(DATA.combat.winner != null){
        endGame();
    }
}

function disableActionButtons(){
    $('#fighterAttack').attr('disabled', true);
    $('#fighterCounter').attr('disabled', true);
    $('#fighterHeal').attr('disabled', true);
    $('#fighterInterrupt').attr('disabled', true);
}
function enableActionButtons(){
    $('#fighterAttack').attr('disabled', false);
    $('#fighterCounter').attr('disabled', false);
    $('#fighterHeal').attr('disabled', false);
    $('#fighterInterrupt').attr('disabled', false);
}

function updateHealthBar(character, actualHealth, initHealth, selfBar){
if(selfBar){
    if(character.attr('id') == 'computer'){
            applyChange("computer",actualHealth, initHealth);
        }else{
            applyChange("fighter",actualHealth, initHealth);
    }
}else{
    if(character.attr('id') == 'computer'){
            applyChange("fighter",actualHealth, initHealth);
        }else{
            applyChange("computer",actualHealth, initHealth);
        }
    }
}

function endGame(){
    disableActionButtons();
    setTimeout(function(){
        Swal.fire({
            title: "END GAME",
            onClose: () => {
                window.location.href = '/combats';
            },
            backdrop: `
                rgba(0,0,123,0.4)
            `
          })
    }, 1000);
}

function skipAnimations(){
    swapCharacterImg(FIGHTER, fighterIdle);
    swapCharacterImg(COMPUTER, computerIdle);
    applyChange("fighter",DATA.combat.salutActualChar1,DATA.combat.salutInitChar1);
    applyChange("computer",DATA.combat.salutActualChar2,DATA.combat.salutInitChar2);
    enableActionButtons();
}