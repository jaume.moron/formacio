package collections;

import collections.domain.Persona;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class CollectionsTest {

    public CollectionsTest() {
    }

    @Test
    public void list(){
        Persona p1 = new Persona("00000001", "Nom1", "Cognoms1");
        Persona p2 = new Persona("00000002", "Nom2", "Cognoms2");
        Persona p3 = new Persona("00000003", "Nom3", "Cognoms3");
        List<Persona> list = new ArrayList<>();
        list.add(p1);
        list.add(p2);
        list.add(p3);
        // TODO 1: Recòrrer tots els registres i validar que a la posició 1 hi ha la persona amb dni 00000001, a la dos, el 00000002, etc.
        //  a) de forma normal (for....)
        //  b) amb un iterador (métode list.iterator()) --> A l'Idea prem Ctrl + j i escriu "itit"
        Persona personaPos0 = list.get(0);

        for(int i=0; i<list.size(); i++){
            Persona personaI = list.get(i);
            if(personaPos0.getDni() == personaI.getDni()) {
                assertTrue(personaPos0.getDni() == personaI.getDni());
            }
        }
//        JAUME: Molt millor així el recòrrer una llista
        for(Persona personaI: list){
            if(personaPos0.getDni() == personaI.getDni()) {
                assertTrue(personaPos0.getDni() == personaI.getDni());
            }
        }
//        JAUME: El que demanava era això: El que has fet només valida que la persona existeix a la llista, no que sigui el primer element de la llista
        assertEquals(p1.getDni(), list.get(0).getDni());

        // JAUME: OK: l'Iterador està bé.
        Iterator<Persona> iterator = list.iterator();
        while (iterator.hasNext()) {
            Persona personaIt = iterator.next();

            if(personaPos0.getDni() == personaIt.getDni()){
                assertTrue(personaPos0.getDni() == personaIt.getDni());
            }

        }
        // TODO 2: Conèixer què fan els següents métodes i posar exemples
        assertTrue(!list.isEmpty());

        Persona persona0 = list.get(0);
        System.out.println(persona0.getDni());

        list.clear();
        assertTrue(list.isEmpty());

        /*list.isEmpty()
        list.get()
        list.clear();*/
        // TODO 3: Conèixer els métodes següent (treballar amb métode equals i hashCode de l'objecte persona) --> A la classe persona prem Alt + Insert
        //recarrego List
        list.add(p1);
        list.add(p2);
        list.add(p3);

        /* JAUME: Hashcode no interessa
        int hashCode0 = persona0.hashCode();
        int hashCodeListObject = list.get(0).hashCode();
        assertTrue(hashCode0 ==  hashCodeListObject);*/

        assertTrue(persona0.equals(list.get(0)));
        assertTrue(0 == persona0.compareTo(list.get(0)));

        /*list.indexOf()*/
        int indexP1 = list.indexOf(p2);
        assertTrue("00000002" == list.get(indexP1).getDni());
        /*list.contains()*/
        assertTrue("Existeix la Persona 3", list.contains(p3));
        /*list.remove()*/
        list.remove(2);
//        o així
//        list.remove(p3)
        assertTrue("No existeix la Persona 3", !list.contains(p3));
        // TODO 4: Ordenar un list (interfície Comparable a la classe Persona i classe Collections)
        Collections.sort(list);

    }

    @Test
    public void map(){
        Map<String, Persona> map = new HashMap<>();
        Persona p1 = new Persona("00000001", "Nom1", "Cognoms1");
        Persona p2 = new Persona("00000002", "Nom2", "Cognoms2");
        Persona p3 = new Persona("00000003", "Nom3", "Cognoms3");
        map.put(p1.getDni(), p1);
        map.put(p2.getDni(), p2);
        map.put(p3.getDni(), p3);
        // TODO 1: Recòrrer tots els registres
        Set<String> code = map.keySet();
        Iterator<String> iterator = code.iterator();

        while (iterator.hasNext()) {
            String dni = iterator.next();
            Persona persona = map.get(dni);
        }

        // TODO 2: Conèixer què fan els següents métodes i posar exemples
        assertTrue(map.isEmpty() == false);
        Persona mapPersona1 = map.get(p1.getDni());
        map.clear();
        assertTrue(map.isEmpty() == true);
//        recarrego el Map
        map.put(p1.getDni(), p1);
        map.put(p2.getDni(), p2);
        map.put(p3.getDni(), p3);
        // TODO 3: Obtenir el registre de la persona amb dni 00000001
//        Ho faig amb un while recorrent el map, pero potser seria mes optim fer servir alguna d'aquestes funcions que he trobat
//        https://www.techiedelight.com/get-map-key-from-value-java/
        if(map.containsValue(p1)){
            code = map.keySet();
            iterator = code.iterator();
            while (iterator.hasNext()) {
                String dni = iterator.next();
                Persona persona = map.get(dni);
                if(persona.getDni() == "00000001"){
                    System.out.println(dni + " => " + persona.getNom() + " > " + persona.getCognoms());
                }
            }
        }
        // JAUME: T'has liat, només cal fer això
        Persona retval = map.get("00000001");
        System.out.println(retval.getDni() + " => " + retval.getNom() + " > " + retval.getCognoms());

        // TODO 4: Eliminar un element del map
        map.remove(p1.getDni());
        System.out.println(map);
    }
}
