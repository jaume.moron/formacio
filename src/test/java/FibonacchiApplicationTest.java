import fibonacci.FibonacchiApplication;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class FibonacchiApplicationTest {

    @Test
    public void Fibonacchi(){
        FibonacchiApplication fibonacchiApplication = new FibonacchiApplication();
        assertEquals(89, fibonacchiApplication.initFibonacci(0, 10));
    }
    @Test
    public void Factorial(){
        FibonacchiApplication fibonacchiApplication = new FibonacchiApplication();
        assertEquals(720, fibonacchiApplication.factorial(6));
    }


}
