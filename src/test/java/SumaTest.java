import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SumaTest {

    @Test
    public void suma(){
        Suma suma = new Suma();
        assertEquals(4, suma.execute(2,2));
        assertEquals(3, suma.execute(4,-1));
    }
}
