package notificacions;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class NotificadorServiceTest {
    // Aquest test comproba que es poden enviar missatges als clients via notificacions push
    @Test
    public void sendMessageUsingPush() {
        // Instanciem el tipus de notificador
        NotificadorPush push = new NotificadorPush();
        // Al motor de notificacions li indiquem quin notificador ha de fer servir
        NotificadorService service = new NotificadorService(push);
        assertTrue(service.sendMessage("assumpte", "cos"));
    }

    // Aquest test comproba que es poden enviar missatges als clients via email
    @Test
    public void sendMessageUsingEmail() {
        // Instanciem el tipus de notificador
        NotificadorEmail email = new NotificadorEmail();
        // Al motor de notificacions li indiquem quin notificador ha de fer servir
        NotificadorService service = new NotificadorService(email);
        assertTrue(service.sendMessage("assumpte", "cos"));
    }

    // Aquest test comproba que es poden enviar missatges als clients via SMS
    // Com el notificador SMS està desactivat, el test espera que falli per una excepció de tipus IllegalArgumentException
    @Test(expected = IllegalArgumentException.class)
    public void sendMessageUsingSms() {
        // Instanciem el tipus de notificador
        NotificadorSMS sms = new NotificadorSMS();
        // Al motor de notificacions li indiquem quin notificador ha de fer servir
        NotificadorService service = new NotificadorService(sms);
        // Com el métode llença una excepció, no cal comprobar la resposta (true o false) perque no s'obtindrà mai
        service.sendMessage("assumpte", "cos");
    }


}
