package cat.jaumemoron.formacio.davidgame.utils;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SecurityUtilsTest {

    @Test(expected = AccessDeniedException.class)
    public void userNotLogged(){
        SecurityUtils.getUsername();
    }

    @Test(expected = AccessDeniedException.class)
    @WithAnonymousUser
    public void anonyousUser(){
        SecurityUtils.getUsername();
    }

    @Test
    @WithUserDetails("david@davidgame.cat")
    public void adminUser(){
        assertEquals("0", SecurityUtils.getId());
        assertEquals("david@davidgame.cat", SecurityUtils.getUsername());
        assertEquals("Test User David", SecurityUtils.getAlias());
        assertEquals("ROLE_USER", SecurityUtils.getRole());
        assertTrue(SecurityUtils.userHasRole("ROLE_USER"));
    }


}
