package cat.jaumemoron.formacio.davidgame.adapter;

import cat.jaumemoron.formacio.davidgame.domain.Enrollment;
import cat.jaumemoron.formacio.davidgame.domain.User;
import cat.jaumemoron.formacio.davidgame.exceptions.UserExistsException;
import cat.jaumemoron.formacio.davidgame.service.EnrollmentsService;
import cat.jaumemoron.formacio.davidgame.service.UsersService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.ui.ConcurrentModel;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UsersAdapterTest {

    @InjectMocks
    private UsersAdapter adapter;

    @Mock
    private UsersService usersService;
    @Mock
    private EnrollmentsService enrollmentsService;
    @Mock
    private JavaMailSender javaMailSender;

    @Test
    public void registerUser() throws UserExistsException {
        // Configuring Mocks
        User user = new User();
        user.setId("1000");
        user.setUsername("david@davidgame.cat");
        user.setAlias("outlaw");
        Mockito.when(usersService.create(ArgumentMatchers.anyString(), ArgumentMatchers.anyString(), ArgumentMatchers.anyString())).thenReturn(user);
        Enrollment enrollment = new Enrollment();
        enrollment.setToken("jdfaw9874canohfhoaswf598hwfhswdfhqw38f7");
        Mockito.when(enrollmentsService.create(ArgumentMatchers.anyString())).thenReturn(enrollment);
        Mockito.doNothing().when(javaMailSender).send(ArgumentMatchers.any(SimpleMailMessage.class));
        ConcurrentModel model = new ConcurrentModel();
        // Provem el métode
        adapter.userRegistration("username", "password", "alias", model);
    }

    @Test
    public void endUserRegister() {
        String token = "jdfaw9874canohfhoaswf598hwfhswdfhqw38f7";
        // Configuring Mocks
        Enrollment enrollment = new Enrollment();
        enrollment.setToken(token);
        Mockito.when(enrollmentsService.findByToken(ArgumentMatchers.anyString())).thenReturn(enrollment);
        Mockito.doNothing().when(usersService).activate(ArgumentMatchers.anyString());
        Mockito.doNothing().when(enrollmentsService).delete(ArgumentMatchers.any(Enrollment.class));
        // Provem el métode
        adapter.endUserRegistration(token);
    }

    @Test(expected = IllegalArgumentException.class)
    public void endUserRegisterFails() {
        adapter.endUserRegistration(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void endUserRegisterFailsEnrollmentDoesNotExists() {
        String token = "jdfaw9874canohfhoaswf598hwfhswdfhqw38f7";
        // Configuring Mocks
        Enrollment enrollment = new Enrollment();
        enrollment.setToken(token);
        Mockito.when(enrollmentsService.findByToken(ArgumentMatchers.anyString())).thenReturn(null);
        // Provem el métode
        adapter.endUserRegistration(token);
    }

    @Test
    public void removeOldEnrollments() {
        // Configuring Mocks
        Enrollment enrollment = new Enrollment();
        enrollment.setId("1000");
        enrollment.setToken("jdfaw9874canohfhoaswf598hwfhswdfhqw38f7");
        enrollment.setUserId("2000");
        List<Enrollment> list = new ArrayList<>();
        list.add(enrollment);
        Mockito.when(enrollmentsService.findByCreationDateLessThan(ArgumentMatchers.any(LocalDate.class))).thenReturn(list);
        Mockito.doNothing().when(enrollmentsService).delete(ArgumentMatchers.any(Enrollment.class));
        Mockito.doNothing().when(usersService).deleteById(ArgumentMatchers.anyString());
        // Provem el métode
        adapter.removeOldEnrollments();
    }

    @Test(expected = UserExistsException.class)
    public void UserRegisterFails() throws UserExistsException {
        ConcurrentModel model = new ConcurrentModel();
        User user = new User();
        user.setId("1001");
        user.setUsername("davidTest@davidgame.cat");
        user.setAlias("outlaw");
        /* Jaume: 1)
            Si tenim això comentat, en el métode constarà que l'usuari no s'ha trobat a la Bdd. Si no donem un
            comportament a un métode de un mock, qual se'l cridi retornarà sempre null
         */
        Enrollment enrollment = new Enrollment();
        enrollment.setToken("jdfaw9874canohfhoaswf598hwfhswdfhqw38f7");
        Mockito.when(usersService.findByUsername(ArgumentMatchers.anyString())).thenReturn(user);
        adapter.userRegistration(user.getUsername(), user.getPassword(), user.getAlias(), model);
         /* Jaume: 6)
            Quan fem un test amb mocks, hem de saber a quins serveis criden el métode que testegem perque sol passar
            que ens deixem de mockejar-ne un i ens falla.
            En aquest cas, si fem mofk del usersService.findByUsername, com és el primer en executar-se i després no en fa cap més
            no cal fer mock de la resta. Això ho soabem perque coneixem el codi que estem testejant
         */
    }
}
