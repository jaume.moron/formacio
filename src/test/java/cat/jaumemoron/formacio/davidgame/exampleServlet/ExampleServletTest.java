package cat.jaumemoron.formacio.davidgame.exampleServlet;

import cat.jaumemoron.formacio.davidgame.constants.CombatStatus;
import cat.jaumemoron.formacio.davidgame.controller.api.CharacterController;
import cat.jaumemoron.formacio.davidgame.domain.Character;
import cat.jaumemoron.formacio.davidgame.domain.Combat;
import cat.jaumemoron.formacio.davidgame.service.CharactersService;
import cat.jaumemoron.formacio.davidgame.service.CombatsService;
import cat.jaumemoron.formacio.davidgame.servlet.ExampleServlet;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ExampleServletTest {
    @InjectMocks
    private ExampleServlet servlet;

    @Mock
    private CombatsService combatsService;

    @Test
    @WithUserDetails("admin@davidgame.cat")
    public void doGet() throws ServletException, IOException {
        // Configuring Mocks
        HttpServletRequest req = mock(HttpServletRequest.class);
        HttpServletResponse resp = mock(HttpServletResponse.class);
        PrintWriter writer = resp.getWriter();
        Mockito.when(resp.getWriter()).thenReturn(writer);

        ExampleServlet servlet = new ExampleServlet();
        servlet.doGet(req, resp);
        assertEquals(0, resp.getStatus());
        List<Combat> CombatInProcess = combatsService.findCombatByStatus(CombatStatus.IN_PROCESS);
        Mockito.when(combatsService.findCombatByStatus(CombatStatus.IN_PROCESS)).thenReturn(CombatInProcess);
    }
}