package cat.jaumemoron.formacio.davidgame;

import cat.jaumemoron.formacio.davidgame.constants.ActionType;
import cat.jaumemoron.formacio.davidgame.constants.CombatStatus;
import cat.jaumemoron.formacio.davidgame.domain.Combat;
import cat.jaumemoron.formacio.davidgame.domain.Torn;
import cat.jaumemoron.formacio.davidgame.service.CombatEngineService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.SecureRandom;

public class CombatSimulation {

    private static final Logger LOGGER = LoggerFactory.getLogger(CombatEngineService.class);
    private CombatEngineService combatEngineService;

    public CombatSimulation(CombatEngineService combatEngineService) {
        this.combatEngineService = combatEngineService;
    }

    public boolean doCombat(Combat combat){
        if(combat.getStatus() != CombatStatus.IN_PROCESS){
            combat.setStatus(CombatStatus.IN_PROCESS);
            LOGGER.debug("COMBAT STATUS SET [{}]!", combat.getStatus());
        }

        Torn torn = combatEngineService.accioResult(combat, getRandomAction(), getRandomAction());
        if(combatEngineService.isCombatFinished(torn))
        {
            combat.setStatus(CombatStatus.FINISHED);
            LOGGER.debug("COMBAT STATUS SET [{}]!", combat.getStatus());
            return true;
        }else{
           doCombat(combat);
        }
        return false;
    }

    private ActionType getRandomAction(){
        ActionType actionType = null;
        SecureRandom rand = new SecureRandom(); //instance of random class
        int upperbound = 100;
        int intRandom = rand.nextInt(upperbound) + 1;
        if(intRandom <= 40){
            actionType = ActionType.ATTACK;
        }
        if(intRandom > 40 && intRandom <= 65){
            actionType = ActionType.COUNTER;
        }
        if(intRandom > 65 && intRandom <= 88){
            actionType = ActionType.HEAL;
        }
        if(intRandom > 88 && intRandom <= 100){
            actionType = ActionType.INTERRUPT;
        }
        return actionType;
    }
}
