package cat.jaumemoron.formacio.davidgame.actions;

import cat.jaumemoron.formacio.davidgame.constants.ActionType;
import cat.jaumemoron.formacio.davidgame.domain.Character;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ActionEngineTest {

    @Test
    public void getAction(){
        Character char1 = new Character();
        char1.setName("char1");
        Character char2 = new Character();
        char2.setName("char2");
        GenericAction action = ActionEngine.getAction(ActionType.ATTACK, char1, char2);
        assertEquals(char1.getName(), action.getSource().getName());
        assertEquals(char2.getName(), action.getTarget().getName());
        assertEquals(ActionType.ATTACK, action.getType());
        action = ActionEngine.getAction(ActionType.COUNTER, char1, char2);
        assertEquals(ActionType.COUNTER, action.getType());
        action = ActionEngine.getAction(ActionType.HEAL, char1, char2);
        assertEquals(ActionType.HEAL, action.getType());
        action = ActionEngine.getAction(ActionType.INTERRUPT, char1, char2);
        assertEquals(ActionType.INTERRUPT, action.getType());
    }


}
