package cat.jaumemoron.formacio.davidgame.controllers;

import cat.jaumemoron.formacio.davidgame.adapter.UsersAdapter;
import cat.jaumemoron.formacio.davidgame.constants.RequestMappingConstants;
import cat.jaumemoron.formacio.davidgame.controller.CharactersController;
import cat.jaumemoron.formacio.davidgame.controller.RegisterController;
import cat.jaumemoron.formacio.davidgame.domain.Character;
import cat.jaumemoron.formacio.davidgame.domain.User;
import cat.jaumemoron.formacio.davidgame.exceptions.UserExistsException;
import cat.jaumemoron.formacio.davidgame.service.CharactersService;
import cat.jaumemoron.formacio.davidgame.service.UsersService;
import cat.jaumemoron.formacio.davidgame.utils.SecurityUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.ui.ConcurrentModel;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static cat.jaumemoron.formacio.davidgame.constants.ErrorMessages.USER_DOES_NOT_EXISTS;
import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CharactersControllerTest {

    @InjectMocks
    private CharactersController controller;

    @Mock
    CharactersService charactersService;

    @Mock
    UsersService usersService;

    @Test
    @WithUserDetails("admin@davidgame.cat")
    public void init(){
        ConcurrentModel model = new ConcurrentModel();

        User userLogged = new User();
        Mockito.when(usersService.findByUsername(ArgumentMatchers.anyString())).thenReturn(userLogged);

        String retval = controller.init(model);
        assertEquals(RequestMappingConstants.INIT_CHARACTERS_REQUEST_MAPPING, retval);
    }
}