package cat.jaumemoron.formacio.davidgame.controllers.api;

import cat.jaumemoron.formacio.davidgame.controller.api.CharacterController;
import cat.jaumemoron.formacio.davidgame.domain.Character;
import cat.jaumemoron.formacio.davidgame.exceptions.UserExistsException;
import cat.jaumemoron.formacio.davidgame.service.CharactersService;
import cat.jaumemoron.formacio.davidgame.utils.SecurityUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CharacterControllerTest {

    @InjectMocks
    private CharacterController controller;

    @Mock
    private CharactersService charactersService;

    @Test
    @WithUserDetails("admin@davidgame.cat")
    public void findAll() throws UserExistsException {

        List<Character> mockedList = new ArrayList<>();
        Mockito.when(charactersService.findAll()).thenReturn(mockedList);

        List<Character> list = controller.findAll();
        assertNotNull(list);
        assertTrue(list.isEmpty());

    }
    @Test
    @WithUserDetails("david@davidgame.cat")
    public void findLoggedChars() throws UserExistsException {
        List<Character> mockedList = new ArrayList<>();
        String id = SecurityUtils.getId();
        Mockito.when(charactersService.findByIdUser(id)).thenReturn(mockedList);

        List<Character> list = controller.findLoggedChars();
        assertNotNull(list);
        assertTrue(list.isEmpty());

    }

}
