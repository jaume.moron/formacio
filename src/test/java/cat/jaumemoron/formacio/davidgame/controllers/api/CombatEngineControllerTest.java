package cat.jaumemoron.formacio.davidgame.controllers.api;

import cat.jaumemoron.formacio.davidgame.constants.ActionType;
import cat.jaumemoron.formacio.davidgame.constants.CombatStatus;
import cat.jaumemoron.formacio.davidgame.controller.api.CombatEngineController;
import cat.jaumemoron.formacio.davidgame.domain.Combat;
import cat.jaumemoron.formacio.davidgame.domain.Torn;
import cat.jaumemoron.formacio.davidgame.service.CombatEngineService;
import cat.jaumemoron.formacio.davidgame.service.CombatsService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CombatEngineControllerTest {
    @InjectMocks
    private CombatEngineController controller;

    @Mock
    private CombatsService combatsService;

    @Mock
    private CombatEngineService combatEngineService;


    @Test
    @WithUserDetails("admin@davidgame.cat")
    public void accioResult()
    {

        // TODO: NO entenc perque treballes amb dos objectes Combat quan només cal 1 amb l'estat pending.
        //  Pensa:
        //  1) Que vull que provi aquest test
        //  2) Quins métodes necessito simular-ne el comportament (mock)?
        //  3) Que espero que em retorn el test?

        // Configuring Mocks
        Combat combatPending = new Combat();
        combatPending.setId("pending");
        combatPending.setStatus(CombatStatus.PENDING);

        Combat combatProcess = new Combat();
        combatProcess.setId("process");
        combatProcess.setStatus(CombatStatus.IN_PROCESS);

        Torn torn = new Torn();
        torn.setId("torn");
        torn.setCombat(combatProcess);

        Mockito.when(combatsService.findById(ArgumentMatchers.anyString())).thenReturn(combatPending);

        // TODO: No posis que fagi EQ de les accions perque la segona acció es calcula aleatoriament (Linisa 54 CombatEnginController) És per això que, en alguns casos, funciona el test
        Mockito.when(combatEngineService.accioResult(ArgumentMatchers.eq(combatProcess), ArgumentMatchers.any(ActionType.class), ArgumentMatchers.any(ActionType.class))).thenReturn(torn);
        Mockito.when(combatsService.setCombatStatus(ArgumentMatchers.any(Combat.class), ArgumentMatchers.any(CombatStatus.class))).thenReturn(combatProcess);
        Mockito.when(combatEngineService.isCombatFinished(ArgumentMatchers.any(Torn.class))).thenReturn(ArgumentMatchers.any(Boolean.class));

        // TODO: Aquest if es innecessarì --> Tu estàs donant aquest estat a l'objecte combatPending
//        if(combatPending.getStatus().equals(CombatStatus.PENDING)){
//
//            // TODO: Aquest assert sobra: Mockito MAI modificarà cap objecte, per tant, combatProcess sempre tindrà aquest valor
//            assertEquals(CombatStatus.IN_PROCESS, combatProcess.getStatus());
//        }

        //TODO: Faltaria que fessin mock de service.isCombatFinished

        // Validate method
        Torn tornEvaluated = controller.accioResult(combatProcess.getId(), 1);
        assertNotNull(tornEvaluated);
        assertEquals(torn.getCombat().getId(), combatProcess.getId());
    }
}
