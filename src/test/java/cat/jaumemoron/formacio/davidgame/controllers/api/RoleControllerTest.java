package cat.jaumemoron.formacio.davidgame.controllers.api;

import cat.jaumemoron.formacio.davidgame.controller.api.RoleController;
import cat.jaumemoron.formacio.davidgame.domain.RoleType;
import cat.jaumemoron.formacio.davidgame.service.RoleTypesService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RoleControllerTest {

    @InjectMocks
    private RoleController controller;

    @Mock
    private RoleTypesService service;

    @Test
    public void findAll() {
        // Configuring Mocks
        List<RoleType> mockedList = new ArrayList<>();
        Mockito.when(service.findAll()).thenReturn(mockedList);
        // Provem el métode
        List<RoleType> list = controller.findAll();
        assertNotNull(list);
        assertTrue(list.isEmpty());
    }

    @Test
    public void create() {
        // Configuring Mocks
        RoleType roleType = new RoleType();
        roleType.setId("1");
        Mockito.when(service.create(ArgumentMatchers.anyString(), ArgumentMatchers.anyLong(),
                ArgumentMatchers.anyLong(), ArgumentMatchers.anyLong(), ArgumentMatchers.anyLong(),
                ArgumentMatchers.anyLong(), ArgumentMatchers.anyLong(), ArgumentMatchers.anyLong(),
                ArgumentMatchers.anyLong())).thenReturn(roleType);
        // Provem el métode
        RoleType roleType2 =  controller.create("name", 0L, 0L,0L,0L,0L,0L,0L,0L);
        assertEquals(roleType.getId(), roleType2.getId());
    }

    @Test
    public void update() {
        // Configuring Mocks
        RoleType roleType = new RoleType();
        roleType.setId("1");
        Mockito.when(service.update(ArgumentMatchers.anyString(), ArgumentMatchers.anyLong(),
                ArgumentMatchers.anyLong(), ArgumentMatchers.anyLong(), ArgumentMatchers.anyLong(),
                ArgumentMatchers.anyLong(), ArgumentMatchers.anyLong(), ArgumentMatchers.anyLong(),
                ArgumentMatchers.anyLong())).thenReturn(roleType);
        // Provem el métode
        RoleType roleType2 =  controller.update("1",10L,20L,30L,40L,50L,60L,70L,80L);
        // Validar resultat
        assertEquals(roleType.getId(), roleType2.getId());
    }

    @Test
    public void delete() throws IllegalArgumentException  {
        // Configuring Mocks
        String id = "1";
        Mockito.doNothing().when(service).deleteById(ArgumentMatchers.anyString());
        // Provem el métode
        controller.delete(id);
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateFails() {
        // Provem el métode
        controller.update("0",null, null, null, null, null, null, null, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateFails1() {
        // Provem el métode
        controller.update("1",null, null, null, null, null, null, null, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateFails2() {
        // Provem el métode
        controller.update("1",0L, null, null, null, null, null, null, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateFails3() {
        // Provem el métode
        controller.update("1",1L, null, null, null, null, null, null, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateFails4() {
        // Provem el métode
        controller.update("1",1L, 0L, null, null, null, null, null, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateFails5() {
        // Provem el métode
        controller.update("1",1L, 1L, null, null, null, null, null, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateFails6() {
        // Provem el métode
        controller.update("1",1L, 1L, 0L, null, null, null, null, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateFails7() {
        // Provem el métode
        controller.update("1",1L, 1L, 1L, null, null, null, null, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateFails8() {
        // Provem el métode
        controller.update("1",1L, 1L, 1L, 0L, null, null, null, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateFails0() {
        // Provem el métode
        controller.update("1",1L, 1L, 1L, 1L, null, null, null, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateFails10() {
        // Provem el métode
        controller.update("1",1L, 1L, 1L, 1L, 0L, null, null, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateFails11() {
        // Provem el métode
        controller.update("1",1L, 1L, 1L, 1L, 1L, null, null, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateFails12() {
        // Provem el métode
        controller.update("1",1L, 1L, 1L, 1L, 1L, 0L, null, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateFails13() {
        // Provem el métode
        controller.update("1",1L, 1L, 1L, 1L, 1L, 1L, null, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateFails14() {
        // Provem el métode
        controller.update("1",1L, 1L, 1L, 1L, 1L, 1L, 0L, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateFails15() {
        // Provem el métode
        controller.update("1",1L, 1L, 1L, 1L, 1L, 1L, 1L, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateFails16() {
        // Provem el métode
        controller.update("1",1L, 1L, 1L, 1L, 1L, 1L, 1L, 0L);
    }
}
