package cat.jaumemoron.formacio.davidgame.controllers;

import cat.jaumemoron.formacio.davidgame.adapter.UsersAdapter;
import cat.jaumemoron.formacio.davidgame.constants.RequestMappingConstants;
import cat.jaumemoron.formacio.davidgame.controller.RegisterController;
import cat.jaumemoron.formacio.davidgame.exceptions.UserExistsException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.ui.ConcurrentModel;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RegisterControllerTest {

    @InjectMocks
    private RegisterController controller;

    @Mock
    private UsersAdapter adapter;

    @Test
    public void init(){
        String retval = controller.init();
        assertEquals(RequestMappingConstants.INIT_REGISTER_REQUEST_MAPPING, retval);
    }

    @Test
    public void register(){
        // Configure mocks
        ConcurrentModel model = new ConcurrentModel();
        try{
            Mockito.doNothing().when(adapter).userRegistration(ArgumentMatchers.anyString(), ArgumentMatchers.anyString(), ArgumentMatchers.anyString(), ArgumentMatchers.any(ConcurrentModel.class));
        }catch (UserExistsException e){

        }
        String retval = controller.register("username", "passworwd", "alias", model);
        assertEquals(RequestMappingConstants.END_REGISTER_REQUEST_MAPPING, retval);
    }

    @Test
    public void endUserRegistration(){
        // Configure mocks
        ConcurrentModel model = new ConcurrentModel();
        try{
            Mockito.doNothing().when(adapter).userRegistration(ArgumentMatchers.anyString(), ArgumentMatchers.anyString(), ArgumentMatchers.anyString(), ArgumentMatchers.any(ConcurrentModel.class));
        }catch (UserExistsException e){

        }
        String retval = controller.endUserRegistration("token", model);
        assertEquals(RequestMappingConstants.INIT_LOGIN_REQUEST_MAPPING, retval);
        assertEquals("Registre completat", model.getAttribute("message"));
    }

}
