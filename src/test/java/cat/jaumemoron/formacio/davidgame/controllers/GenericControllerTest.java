package cat.jaumemoron.formacio.davidgame.controllers;

import cat.jaumemoron.formacio.davidgame.constants.RequestMappingConstants;
import cat.jaumemoron.formacio.davidgame.controller.GenericController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GenericControllerTest {

    @Autowired
    private GenericController controller;

    @Test
    public void root(){
        String retval = controller.root();
        assertEquals(RequestMappingConstants.INIT_ROOT_REQUEST_MAPPING, retval);
    }

    @Test
    public void login(){
        String retval = controller.login();
        assertEquals(RequestMappingConstants.INIT_LOGIN_REQUEST_MAPPING, retval);
    }
}
