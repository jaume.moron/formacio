package cat.jaumemoron.formacio.davidgame.service.impl;

import cat.jaumemoron.formacio.davidgame.domain.Character;
import cat.jaumemoron.formacio.davidgame.domain.Combat;
import cat.jaumemoron.formacio.davidgame.service.CharactersService;
import cat.jaumemoron.formacio.davidgame.service.CombatsService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CombatsServiceTest {
    @Autowired
    private CombatsService combatsService;
    @Autowired
    private CharactersService charactersService;

    @Test
    public void AllMethods(){
        Character character0 = charactersService.findById("0");
        Character character1 = charactersService.findById("1");
        Character character2 = charactersService.findById("2");

        Combat combat1 = combatsService.create(character0, character1);
        Combat combat2 = combatsService.create(character1, character2);
        List<Combat> listCharacter0 = combatsService.findAllByRol(character0, character0);
        List<Combat> listCharacter1 = combatsService.findAllByRol(character1, character1);
        List<Combat> listCharacter2 = combatsService.findAllByRol(character2, character2);

        assertEquals(2, listCharacter0.size());
        assertEquals(3, listCharacter1.size());
        assertEquals(1, listCharacter2.size());
        //agafem tots els combats
        List<Combat> listCombats = combatsService.findAll();
        assertEquals(3, listCombats.size());
        //agafem el primer creat
        Combat combat1Get = combatsService.findById("0");

        assertNotEquals(combat1, combat1Get);
        //eliminem el combat2
        combatsService.deleteById(combat2.getId());
    }

    @Test(expected = IllegalArgumentException.class)
    public void findByIdFails() {
        combatsService.findById(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createFails() {
        Character character0 = charactersService.findById("0");
        combatsService.create(character0, null);
        combatsService.create(null, character0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void deleteByIdFails() {
        combatsService.deleteById(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void findAllByRol1Fails() {
        Character character0 = charactersService.findById("0");
        combatsService.findAllByRol(character0, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void findAllByRol2Fails() {
        Character character0 = charactersService.findById("0");
        combatsService.findAllByRol(null, character0);
    }
}
