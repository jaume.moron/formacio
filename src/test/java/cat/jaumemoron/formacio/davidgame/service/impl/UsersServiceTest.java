package cat.jaumemoron.formacio.davidgame.service.impl;

import cat.jaumemoron.formacio.davidgame.constants.SecurityConstants;
import cat.jaumemoron.formacio.davidgame.constants.UserStatus;
import cat.jaumemoron.formacio.davidgame.domain.Character;
import cat.jaumemoron.formacio.davidgame.domain.User;
import cat.jaumemoron.formacio.davidgame.service.CharactersService;
import cat.jaumemoron.formacio.davidgame.service.UsersService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UsersServiceTest {

    @Autowired
    private UsersService service;

    @Autowired
    private CharactersService charactersService;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Test
    public void allMethods() {
        // Recuperem el llistat d'usuaris
        List<User> userList = service.findAll();
        int size = userList.size();
        // Comprobem que la llista no és buida
        assertFalse(userList.isEmpty());
        // Comprobem que és el primer element de la llista (el primer és l'admin)
        assertEquals("0", userList.get(1).getId());
        String username = "username";
        String password = "01234567890123456789";
        String alias = "alias";
        // Creem un usuari nou
        User user = service.create(username, password, alias);
        // Validem que s'ha definit el rol d'usuari
        assertEquals(SecurityConstants.USER, user.getRole());
        // Comprobem que l'objecte no és null
        assertNotNull(user);
        // Comprobem que se li ha assignat un id
        assertNotNull(user.getId());
        // Comprobem que els seus atributs són els que hem indicat a la crida
        assertEquals(username, user.getUsername());
        // Comprobem que el password de la Bdd (xifrat) no és el mateix que l'original
        assertNotEquals(password, user.getPassword());
        assertEquals(alias, user.getAlias());
        // Obtenim l'objecte cercant per id
        User user2 = service.findById(user.getId());
        // Comprobem que els dos objectes són idéntics
        assertEquals(user.getId(), user2.getId());
        assertEquals(user.getUsername(), user2.getUsername());
        assertEquals(user.getPassword(), user2.getPassword());
        assertEquals(user.getAlias(), user2.getAlias());
        assertEquals(user.getRole(), user2.getRole());
        assertEquals(UserStatus.PENDING_OF_ENROLLMENT, user2.getStatus());
        // Comprobem que la cerca per nom funciona
        user2 = service.findByUsername(user.getUsername());
        // Comprobem que els dos objectes són idéntics
        assertEquals(user.getId(), user2.getId());
        assertEquals(user.getUsername(), user2.getUsername());
        assertEquals(user.getPassword(), user2.getPassword());
        assertEquals(user.getAlias(), user2.getAlias());
        assertEquals(UserStatus.PENDING_OF_ENROLLMENT, user2.getStatus());

        // Activem l'usuari i comprobem que s'ha canviat l'estat
        service.activate(user2.getId());
        user2 = service.findById(user2.getId());
        assertEquals(UserStatus.ACTIVE, user2.getStatus());
        // Recuperem de nou el llistat d'usuaris
        userList = service.findAll();
        // Comprobem que a la llista d'usuaris apareix el nou usuari
        assertEquals(size + 1, userList.size());
        // Modifiquem l'atribut alies
        String newAlias = "newAlias";
        user = service.changeAlias(user.getId(), newAlias);
        // Comprobem que l'atribut alies s'ha modificat correctament
        assertEquals(newAlias, user.getAlias());
        // Recuperem de nou el llistat d'usuaris
        service.deleteById(user.getId());
        // Comprobem que l'objecte ja no es pot recuperar per id
        User user3 = service.findById(user.getId());
        assertNull(user3);
        // Comprobem que a la llista d'usuaris ja no apareix el nou usuari
        userList = service.findAll();
        assertEquals(size, userList.size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void findByIdFails() {
        service.findById(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createFails() {
        service.create(null, null, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createFails1() {
        service.create("username", null, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createFails2() {
        service.create("username", null, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void changeAliasFails() {
        service.changeAlias(null, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void changeAliasFails2() {
        service.changeAlias("1", null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void deleteByIdFails() {
        service.deleteById(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void activateFails() {
        service.activate(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void findByUsernameFails() {
        service.findByUsername(null);
    }
}
