package cat.jaumemoron.formacio.davidgame.service.impl;

import cat.jaumemoron.formacio.davidgame.domain.RoleType;
import cat.jaumemoron.formacio.davidgame.service.RoleTypesService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RolsTypeServiceTest {
    @Autowired
    private RoleTypesService service;

    @Test
    @Transactional
    @WithMockUser(value = "test", authorities = {"ROLE_ADMIN"}) // Simulem usuari admin perque el servei RoleTypesService està securitzat
    public void AllMethods() throws IllegalArgumentException  {
        service.deleteById("2");
        //creem
        RoleType roleType0 = service.create("rolType0", 30L,45L,50L,80L,60L,12L, 70L, 80L);
        List<RoleType> llistaTypusRol = service.findAll();
        assertFalse(llistaTypusRol.isEmpty());
        assertNotNull(roleType0);
        roleType0.setAttackSucces(90L);
        roleType0.setCounterSucces(20L);
        roleType0.setCastHealSucces(10L);
        roleType0.setInterruptSucces(40L);
        roleType0.setSalut(40L);
        roleType0.setArmadura(10L);
        roleType0.setMal(60L);
        //roleType0.setCastHealPercent(90L);

        long type0ASucces = roleType0.getAttackSucces();
        long type0DSucces = roleType0.getCounterSucces();
        long type0HSucces = roleType0.getCastHealSucces();
        long type0ISucces = roleType0.getInterruptSucces();
        long type0Salut = roleType0.getSalut();
        long type0armor = roleType0.getArmadura();
        long type0mal = roleType0.getMal();
        //long type0HPercent = roleType0.getCastHealPercent();
        assertEquals(90, type0ASucces);
        assertEquals(20, type0DSucces);
        assertEquals(10, type0HSucces);
        assertEquals(40, type0ISucces);
        assertEquals(40, type0Salut);
        assertEquals(10, type0armor);
        assertEquals(60, type0mal);
        //assertEquals(90, type0HPercent);

        roleType0 = service.update(roleType0.getId(), 80L, 10L, 5L, 30L, 30L, 5L, 50L, 80L);
        type0ASucces = roleType0.getAttackSucces();
        type0DSucces = roleType0.getCounterSucces();
        type0HSucces = roleType0.getCastHealSucces();
        type0ISucces = roleType0.getInterruptSucces();
        type0Salut = roleType0.getSalut();
        type0armor = roleType0.getArmadura();
        type0mal = roleType0.getMal();
        //type0HPercent = roleType0.getCastHealPercent();
        assertEquals(80, type0ASucces);
        assertEquals(10, type0DSucces);
        assertEquals(5, type0HSucces);
        assertEquals(30, type0ISucces);
        assertEquals(30, type0Salut);
        assertEquals(5, type0armor);
        assertEquals(50, type0mal);
        //assertEquals(80, type0HPercent);
        // TODO: Aquí hauries d'eliminar un Role creat en aquest test. El role 1 es fa servir en altres tests.
        // TODO: Si l'esborrem fem que els altres tests fallin
        service.deleteById(roleType0.getId());
        RoleType roleType1 = service.findById(roleType0.getId());
        assertNull(roleType1);

    }

    @Test(expected = IllegalArgumentException.class)
    @WithMockUser(value = "test", authorities = {"ROLE_ADMIN"}) // Simulem usuari admin perque el servei RoleTypesService està securitzat
    public void findByIdFails() {
        service.findById(null);
    }

    @Test(expected = IllegalArgumentException.class)
    @WithMockUser(value = "test", authorities = {"ROLE_ADMIN"}) // Simulem usuari admin perque el servei RoleTypesService està securitzat
    public void createFailsAttack() {service.create("rolType", null,0L,0L,0L,0L,0L, 80L, 0L); }
    @Test(expected = IllegalArgumentException.class)
    @WithMockUser(value = "test", authorities = {"ROLE_ADMIN"}) // Simulem usuari admin perque el servei RoleTypesService està securitzat
    public void createFailsDodge() { service.create("rolType", 0L,null,0L,0L,0L,0L,80L,0L); }

    @Test(expected = IllegalArgumentException.class)
    @WithMockUser(value = "test", authorities = {"ROLE_ADMIN"}) // Simulem usuari admin perque el servei RoleTypesService està securitzat
    public void createFailsName() { service.create(null, 0L,0L,0L,0L,0L,0L,80L,0L); }

    @Test(expected = IllegalArgumentException.class)
    @WithMockUser(value = "test", authorities = {"ROLE_ADMIN"}) // Simulem usuari admin perque el servei RoleTypesService està securitzat
    public void deleteByIdFails() throws IllegalArgumentException  {
        service.deleteById(null);
    }
}
