package cat.jaumemoron.formacio.davidgame.service.impl;

import cat.jaumemoron.formacio.davidgame.domain.Character;
import cat.jaumemoron.formacio.davidgame.domain.RoleType;
import cat.jaumemoron.formacio.davidgame.dto.CharacterAttributesModified;
import cat.jaumemoron.formacio.davidgame.service.CharactersService;
import cat.jaumemoron.formacio.davidgame.service.RoleTypesService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CharactersServiceTest {

    @Autowired
    private CharactersService service;
    @Autowired
    private RoleTypesService typeService;

    @Test
    @WithMockUser(value = "test", authorities = {"ROLE_ADMIN"}) // Simulem usuari admin perque el servei RoleTypesService està securitzat
    public void AllMethods(){
        RoleType rogue = typeService.findById("0");
        List<Character> characterList = service.findAll();
        int size = characterList.size();
        List<Character> rogueList = service.findByRolType(rogue);
        int sizeRogue = rogueList.size();
        assertFalse(characterList.isEmpty());
        assertEquals("0", characterList.get(0).getId());
        CharacterAttributesModified attributes = CharacterAttributesModified.Builder.aCharacterAttributesModified()
                .withMal(1L)
                .withArmadura(-2L)
                .withSalut(3L)
                .withHeal(4L).build();
        Character character1 = service.create("0", rogue, "Rol 1", attributes);
        assertNotNull(character1);
        assertEquals(rogue, character1.getRoleType());
        assertEquals(rogue.getMal() + attributes.getMal(), character1.getMal().longValue());
        assertEquals(rogue.getSalut() + attributes.getSalut(), character1.getSalut().longValue());
        assertEquals(rogue.getArmadura() + attributes.getArmadura(), character1.getArmadura().longValue());
        assertEquals(rogue.getHeal() + attributes.getHeal(), character1.getHeal().longValue());
        characterList = service.findAll();
        assertEquals(size+1, characterList.size());
        rogueList = service.findByRolType(rogue);
        assertEquals(sizeRogue+1, rogueList.size());

        service.deleteById(character1.getId());
        character1 = service.findById(character1.getId());
        assertNull(character1);


    }
}
