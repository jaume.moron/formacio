package cat.jaumemoron.formacio.davidgame.service;

import cat.jaumemoron.formacio.davidgame.domain.Enrollment;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EnrollmentsServiceTest {

    @Autowired
    private EnrollmentsService service;


    @Test
    public void allMethods(){
        String userId = "0";
        // Creem l'enrollment
        Enrollment enrollment = service.create(userId);
        assertNotNull(enrollment);
        assertNotNull(enrollment.getToken());
        //Cerquem l'enrollment per id
        //Cerquem l'enrollment pel codi
        Enrollment enrollment2 = service.findByToken(enrollment.getToken());
        assertNotNull(enrollment2);
        assertEquals(enrollment.getId(), enrollment2.getId());
        assertEquals(enrollment.getToken(), enrollment2.getToken());
        // Obtenim tots els enrollments
        List<Enrollment> list = service.findAll();
        assertNotNull(list);
        assertFalse(list.isEmpty());
        assertEquals(enrollment2.getId(), list.get(0).getId());
        // Obtenim tots els enrollments amb data creació inferior a demà (el que hem creat)
        list = service.findByCreationDateLessThan(LocalDate.now().plusDays(1));
        assertNotNull(list);
        assertFalse(list.isEmpty());
        assertEquals(enrollment2.getId(), list.get(0).getId());
        // Obtenim tots els enrollments amb data creació inferior a ahir (cap)
        list = service.findByCreationDateLessThan(LocalDate.now().minusDays(1));
        assertNotNull(list);
        assertTrue(list.isEmpty());
        // Esborrem l'enrollment
        service.delete(enrollment2);
        enrollment2 = service.findByToken(enrollment2.getToken());
        assertNull(enrollment2);
    }


    @Test(expected = IllegalArgumentException.class)
    public void createFails(){
        service.create(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void deleteFails(){
        service.delete(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void findByTokenFails(){
        service.findByToken(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void findByCreationDateLessThanFails(){
        service.findByCreationDateLessThan(null);
    }

}

