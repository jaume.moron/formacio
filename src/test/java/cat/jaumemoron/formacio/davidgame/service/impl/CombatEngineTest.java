package cat.jaumemoron.formacio.davidgame.service.impl;

import cat.jaumemoron.formacio.davidgame.CombatSimulation;
import cat.jaumemoron.formacio.davidgame.domain.Character;
import cat.jaumemoron.formacio.davidgame.domain.Combat;
import cat.jaumemoron.formacio.davidgame.domain.Torn;
import cat.jaumemoron.formacio.davidgame.service.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CombatEngineTest {

    @Autowired
    private TornsService tornsService;
    @Autowired
    private CharactersService charactersService;
    @Autowired
    private RoleTypesService rolTypeService;
    @Autowired
    private CombatsService combatsService;
    @Autowired
    private CombatEngineService combatEngineService;

    @Test
    public void AllMethods(){
        Character rogue = charactersService.findById("0");
        Character paladin = charactersService.findById("1");
        // creem combat (he eliminat els parámetres de salut perque el propi mètode els pot obtenir del rolType)
        Combat combat = combatsService.create(rogue, paladin);

        CombatSimulation combatSimulation = new CombatSimulation(combatEngineService);
        combatSimulation.doCombat(combat);
        combatEngineService.endGame(combat);
        List<Torn> tornsCombat = tornsService.findAllByCombat(combat);
        assertTrue(tornsCombat.size() > 0);


    }
}
