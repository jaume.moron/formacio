package cat.jaumemoron.formacio.davidgame.service.impl;

import cat.jaumemoron.formacio.davidgame.constants.ActionType;
import cat.jaumemoron.formacio.davidgame.domain.Character;
import cat.jaumemoron.formacio.davidgame.domain.Combat;
import cat.jaumemoron.formacio.davidgame.domain.RoleType;
import cat.jaumemoron.formacio.davidgame.domain.Torn;
import cat.jaumemoron.formacio.davidgame.dto.CharacterAttributesModified;
import cat.jaumemoron.formacio.davidgame.service.CharactersService;
import cat.jaumemoron.formacio.davidgame.service.CombatsService;
import cat.jaumemoron.formacio.davidgame.service.RoleTypesService;
import cat.jaumemoron.formacio.davidgame.service.TornsService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TornsServiceTest {

    @Autowired
    private TornsService service;
    @Autowired
    private CombatsService combatsService;
    @Autowired
    private CharactersService charactersService;
    @Autowired
    private RoleTypesService roleTypesService;

    @Test
    @Transactional
    @WithMockUser(value = "test", authorities = {"ROLE_ADMIN"}) // Simulem usuari admin perque el servei RoleTypesService està securitzat
    public void AllMethods(){

        RoleType roleType1 = roleTypesService.create("rolType1", 100L, 70L, 65L, 65L, 40L, 0L, 60L, 90L );
        CharacterAttributesModified attributes = CharacterAttributesModified.Builder.aCharacterAttributesModified()
                .withMal(1L)
                .withArmadura(-2L)
                .withSalut(3L)
                .withHeal(4L).build();
        Character character1 = charactersService.create("0", roleType1, "Rol Torn", attributes);
        Combat combat = combatsService.create(character1, character1);
        Torn torn = service.create(combat, ActionType.ATTACK, ActionType.ATTACK, true, true, 20L, 10L);
        Torn torn1 = service.create(combat, ActionType.ATTACK, ActionType.COUNTER, true, true, 0L, null);

        List<Torn> llistaTornsCombat = service.findAllByCombat(combat);

        assertNotNull("Llistat null i no ho hauria de ser", llistaTornsCombat);

        Torn findedById = service.findById(torn.getId());
        assertNotNull("no hauria de ser NULL", findedById);

        List<Torn> allTorns = service.findAll();
        assertNotNull("no hauria de ser NULL", allTorns);
        service.deleteById(torn1.getId());
        Torn tornDeleted = service.findById(torn1.getId());
        assertNull("ha de ser NULL", tornDeleted);

    }

    @Test(expected = IllegalArgumentException.class)
    public void findByIdFails() {
        service.findById(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createFails() {
        service.create(null, ActionType.ATTACK, ActionType.COUNTER, true, true, 0L, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void findByCombatFails() { service.findAllByCombat(null); }

    @Test(expected = IllegalArgumentException.class)
    public void deleteByIdFails() {
        service.deleteById(null);
    }

}
