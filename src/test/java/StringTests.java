import org.junit.Test;

import static org.junit.Assert.*;

public class StringTests {

    @Test
    public void test(){
        String a = "CASA";
        String b = "CASA";
        String c = a;
        String d = new String("CASA");
        String e = new String("CASA");

        assertEquals(a, b);
        assertEquals(a, c);
        assertEquals(b, c);
        assertEquals(a, d);
        assertEquals(d, e);

        assertTrue(a.equals(b));
        assertTrue(a==c);
        assertTrue(b==c);
        assertFalse(a==d);
        assertFalse(d==e);


    }
}
