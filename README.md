# Teoria
- Repàs
  - Maven
    - mvn clean compile
    - mvn clean package
    - mvn clean install
    - mvn clean deploy
  - Integració continua
  - Jenkins
  - Sonar
  - Spring
  - Hibernate
- Web
  - Servlets
  - JSP's
  - Web services
  - Serveis Rest
  - Microserveis
  - MVC
- Què són els DTO's?

# Coses que he afagit i t'he d'explicar
- Tests amb Mocks
- Enrollments (registre)    
    - Enviament correu (ara es pinta en el log)
    - Caducitat enrollments @EnableScheduling @Schedule
- Plantilles html (dependencia POM thymeleaf)
- Controladors (Spring MVC)   
    - Normals (@Controller)
    - Rest (@RestController)

- Seguretat (dependencia POM)
    - SecurityConfig 
    - login
    - rols i authorities (prefix)
    - @EnableGlobalMethodSecurity -> @PreAuthorize("hasRole('ROLE_ADMIN')") 
    - Diferencia entre  .antMatchers("/api/role/**").hasRole(SecurityConstants.ADMIN) i @PreAuthorize("hasRole('ROLE_ADMIN')")
- Swagger (per testejar controladors Rest)
  - SwaggerConfig
  - http://localhost:8080/swagger-ui.html
  - Anotacions
    - Controladors
    - Entitats 
    
# Què ha de tenir la pàgina web
- Home 
- Rols (llsitat de tipus de rols i atributs)
- Combats (llistar combats ja fets i veure'ls)
- Registre d'usuaris
- Login
- Manteniment d'usuaris (llistat i eliminació) --> Només per l'administrador
- Manteniment de rols --> Només per l'administrador
- Manteniment de combats (eliminació) --> Només per l'administrador
- Gestió de personatges --> Nmés per usuaris logats
- Fer combat --> Nmés per usuaris logats

#Contingut estàtic
  - Dins del directori src/main/resource/META-ING/theme he posat el tema original de boostrap descarregat (no és accessible al codi de l'aplicació')  
  - Tot el que es posi dins del directori src/main/resources/static spring hi publica a l'arrel de l'aplicació.
    - El directori src/main/resources/static/css passa a ser /css
  Els css, js, etc del tema de boostrap els he copiat a src/main/resources/static/ 

#Thymeleaf  
  - Motor de plantilles html
    - namespace `th:` indica que thymeleaf ha de processar aquell tag
        - Sempre, ha començament de cada pàgina, s'ha d'indicar el namespace:
        `<html xmlns:th="http://www.thymeleaf.org" xmlns="http://www.w3.org/1999/xhtml">`
  - Permet a dissenyadors poder tocar pàgines amb codi de programadors sense que es tornin `lelos`
  - Fragments
    Son per poder fer servir troços d'html en diferents pàgines sense haver de repetir el codi
    Poden rebre paràmetres
     - En el nostre fragment `fragments/header :: head`, li passem el title de cada pàgina (això és perque aquest fragment conté tot el que hem de posar dins el tag `<head>`)
     - Un altre exemple: podriem tenir un fragment que pinta una taula amb un estil determinat i com a paràmetre seria la llista de registres de la taula  
    Més informació a https://www.thymeleaf.org/doc/articles/layouts.html 
    `<div th:replace="fragments/header :: head (pageTitle = 'El meu número')" th:remove="tag"/>`
    `th:replace` --> vol dir que el contingut de dins del tag div, s'ha de reemplaçar amb el contingut del tag `head` de la pàgina `header.html` del directori `fragments`
    si tinguèssim `th:insert` afagiriem DINS el tag div, el contingut de `header.html/head`
    `th:remove="tag"` --> vol dir que el tag `div` que indiquem s'ha d'eliminar. Si no ho poséessim, el contingut del fragment es posaria dins del tag div. En el cas del head, allà 
    no pot haver-hi divs, per tant, posem el `th:remove="tag"`
    A la resta de fragments, simplement no ens interessa  
    
    La pàgina de login, de registre i els errors són exemples senzills de com funciona thymeleaf i els fragments
    
    En les nostres pàgines tenim 3 fragments bàsics:
      - el head
      - el header (amb el menú)
      - el footer
      
    Alhora de fer referència a fitxers js, css i imatges es fa de la següent manera:    
    `<link rel="stylesheet" th:href="@{/css/bootstrap.min.css}" href="css/bootstrap.min.css"/>`
    `<script th:src="@{/js/html5shiv.min.js}" src="js/html5shiv.min.js"></script>`
    `<img th:src="@{/img/logo.png}" src="img/logo.png" alt="logo">`
    El tag `th:src` li diu a thymeleaf que sustitueixi el contingut de l'atribut src per la ruta correcte de servidor.
        Recorda que els recursos estatis es troben dins del directori static de src/main/resources però es publique dins de l'arrel (/)
     Mantenir el tag src et permet editar la pàgina, per exemple desde firefox, sense haver d'aixecar l'aplicació. 
    
#Multiidioma (i18n)
  Per a habilitar el multidioma es creen els fitxers de traduccions al directori src/main/resources amb el nom messages.properties. 
  Per cada idioma diferent, es troba un fitxer on el suffixe és el codi de l'idioma. Per exemple:
    - messages_es.properties --> Castellà
    - messages_ca.properties --> Català
    - messages_fr.properties --> Francés
  El fitxer messages.properties conté les traduccions per defecte
  
  Per a fer servir els textos desde thymeleaf es fa així:
  `<h2 th:text="#{login.title}">Login</h2>`
  On login.title és la key del text que volem posar. Amb l'atribut `th:text` li diem a thymeleaf que substitueixi el text/valor del tag amb aquest
  Recorda que si afageixeis o canvies cap text, has de rearrancar l'aplicació
  
  A nivell d'spring, s'ha d'afagir un bean a la classe `WebConfigurer`
      
# Preguntes de certificació Java 
  https://www.myexamcloud.com/onlineexam/viewExam.html?t=r7jP3LXLHz8=
  
# Coses a tenir en compte
  A l' acció de curar s'ha de controlar que no passi de la vida maxima que te el rol <-- Arreglat
  
# Coses a corregir    
  La pantalla de creació de personatge ha de permetre tornar enrere, a la pantalla de llistat de rols
  La pantalla de "Your role has been modified" falta la traducció de textos
  El datatable està sempre en anglés. Ha de fer servir l'idioma de l'usuari
  L'administrador ha de tenir una gestió d'usuaris (llistar i poder eliminar)
  
 #Pantalla del combat
 1- Un desplegable per escollir el meu personatge i un datatable per escollir el contrincant, amb el nom del personatge,l'usuari que l'a creat i el tipus de rol <-- fet
 2- resol.lució del combat. <-- fet 
 3- Modifiacada l'accio del Dodge per CounterAttack, mola més i es mes útil al combat xD
 
 ANIMACIONS
   s'ha d'apurar a les animacions ara son les basiques, pero potser caldria que en el cas del counterattack
   es resolgues amb una animaciò ja que es extrany fer ho per torns.
   a l'hora de curar-se, quan l'altre ataca queda malament l'animació, ja que de veure l'attack i la curacio
   com a accions independents, ara es resol a l'hora i queda extrany
   
#Coses per fer
Fer que l'enviament de correu només es fagi si una propietat del fitxer de configuració (mail.enabled) està activada (true) Si no està activada (false)
el enrollment escriurem al log el token generat (classe Users Adapter)     